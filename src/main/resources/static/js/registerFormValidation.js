const email = document.getElementById("email");
const frname = document.getElementById("frname");
const faname = document.getElementById("faname");
const psw = document.getElementById("psw")

const emailError = document.getElementById("emailError");
const frnameError = document.getElementById("frnameError");
const fanameError = document.getElementById("fanameError");

const pswErrorLow = document.getElementById("pswErrorLow");
const pswErrorUp = document.getElementById("pswErrorUp");
const pswErrorNumber = document.getElementById("pswErrorNumber");
const pswErrorSpecial = document.getElementById("pswErrorSpecial");
const pswErrorLength = document.getElementById("pswErrorLength");

const submitButton = document.getElementById("submit-button");

// As per the HTML Specification
const emailRegExp =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

function hasLowercase(s) {
    const pattern = /[a-z]+/;
    return pattern.test(s);
}

function hasUppercase(s) {
    const pattern = /[A-Z]+/;
    return pattern.test(s);
}

function hasNumber(s) {
    const pattern = /[0-9]+/;
    return pattern.test(s);
}

function hasSpecialCase(s) {
    const pattern = /[@$!%*?&]+/;
    return pattern.test(s);
}

function hasEightChars(s) {
    const pattern = /.{8,}/;
    return pattern.test(s);
}

function checkSubmitPossible() {
    if (email.value.length !== 0 && emailRegExp.test(email.value) && frname.value.length !== 0 && faname.value.length !== 0 && hasLowercase(psw.value) && hasLowercase(psw.value) && hasNumber(psw.value) && hasSpecialCase(psw.value) && hasEightChars(psw.value)) {
        submitButton.disabled = false;
    } else {
        submitButton.disabled = true;
    }
}

// This defines what happens when the user types in the field
email.addEventListener("input", () => {
    const isEmpty = email.value.length === 0
    const isValid = emailRegExp.test(email.value);
    if (isValid && !isEmpty) {
        email.className = "form-control is-valid";
        emailError.textContent = "";
        emailError.className = "error";
    } else if (!isValid && !isEmpty) {
        email.className = "form-control is-invalid";
        emailError.textContent = "Email format invalid";
        emailError.className = "invalid-feedback";
    } else if (isEmpty) {
        email.className = "form-control is-invalid";
        emailError.textContent = "Please enter an email";
        emailError.className = "invalid-feedback";
    } else {
        email.className = "form-control";
        emailError.textContent = "";
        emailError.className = "error";
    }
    checkSubmitPossible();
});

faname.addEventListener("input", () => {
    const isEmpty = faname.value.length !== 0;
    if (isEmpty) {
        faname.className = "form-control is-valid";
        fanameError.textContent = "";
        fanameError.className = "error";
    } else {
        faname.className = "form-control is-invalid";
        fanameError.textContent = "Please enter a family name";
        fanameError.className = "invalid-feedback";
    }
    checkSubmitPossible();
});

frname.addEventListener("input", () => {
    const isEmpty = frname.value.length !== 0;
    if (isEmpty) {
        frname.className = "form-control is-valid";
        frnameError.textContent = "";
        frnameError.className = "error";
    } else {
        frname.className = "form-control is-invalid";
        frnameError.textContent = "Please enter a first name";
        frnameError.className = "invalid-feedback";
    }
    checkSubmitPossible();
});

psw.addEventListener("input", () => {

    if (hasLowercase(psw.value)) {
        pswErrorLow.textContent = "";
        pswErrorLow.className = "error";
    } else {
        pswErrorLow.textContent = "Password must contains at least one lowercase";
        pswErrorLow.className = "invalid-feedback";
    }
    if (hasUppercase(psw.value)) {
        pswErrorUp.textContent = "";
        pswErrorUp.className = "error";
    } else {
        pswErrorUp.textContent = "Password must contains at least one uppercase";
        pswErrorUp.className = "invalid-feedback";
    }
    if (hasNumber(psw.value)) {
        pswErrorNumber.textContent = "";
        pswErrorNumber.className = "error";
    } else {
        pswErrorNumber.textContent = "Password must contains at least one number";
        pswErrorNumber.className = "invalid-feedback";
    }
    if (hasSpecialCase(psw.value)) {
        pswErrorSpecial.textContent = "";
        pswErrorSpecial.className = "error";
    } else {
        pswErrorSpecial.textContent = "Password must contains at least one special character";
        pswErrorSpecial.className = "invalid-feedback";
    }
    if (hasEightChars(psw.value)) {
        pswErrorLength.textContent = "";
        pswErrorLength.className = "error";
    } else {
        pswErrorLength.textContent = "Password must contains at least eight characters";
        pswErrorLength.className = "invalid-feedback";
    }
    if (hasLowercase(psw.value) && hasLowercase(psw.value) && hasNumber(psw.value) && hasSpecialCase(psw.value) && hasEightChars(psw.value)) {
        psw.className = "form-control is-valid";
    } else {
        psw.className = "form-control is-invalid";
    }
    checkSubmitPossible();
});
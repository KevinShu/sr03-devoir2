const email = document.getElementById("email");
const psw = document.getElementById("psw")
const emailError = document.getElementById("emailError");
const submitButton = document.getElementById("submit-button")

// As per the HTML Specification
const emailRegExp =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

function checkLoginPossible() {
    if (emailRegExp.test(email.value) && email.value.length !== 0 && psw.value.length !== 0) {
        submitButton.disabled = false;
    } else {
        submitButton.disabled = true;
    }
}

window.addEventListener("load", () => {
    const isValid = email.value.length === 0 || emailRegExp.test(email.value);
    email.className = isValid ? "form-control" : "form-control is-invalid";
});

email.addEventListener("input", () => {
    const isValid = email.value.length === 0 || emailRegExp.test(email.value);
    if (isValid) {
        email.className = "form-control";
        emailError.textContent = "";
        emailError.className = "error";
    } else {
        email.className = "form-control is-invalid";
        emailError.textContent = "Email format invalid";
        emailError.className = "error active";
    }
    checkLoginPossible();
});

psw.addEventListener("input", () => {
    checkLoginPossible();
});
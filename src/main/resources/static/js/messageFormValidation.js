const adminSelectBox = document.getElementById("selectBox-admin");
const userSelectBox = document.getElementById("selectBox-user");
const adminMsgContent = document.getElementById("messageContent-admin");
const userMsgContent = document.getElementById("messageContent-user");
const adminSendButton = document.getElementById("send-button-admin");
const userSendButton = document.getElementById("send-button-user");

function checkAdminOkToSend() {
    adminSendButton.disabled = !(adminSelectBox.value != null && adminMsgContent != null);
}

function checkUserOkToSend() {
    userSendButton.disabled = !(userSelectBox.value != null && userMsgContent != null);
}

if (adminSelectBox != null) {
    adminSelectBox.addEventListener("change", () => {
        checkAdminOkToSend();
    })
}
if (adminMsgContent != null) {
    adminMsgContent.addEventListener("input", () => {
        checkAdminOkToSend();
    })
}

if (userSelectBox != null) {
    userSelectBox.addEventListener("change", () => {
        checkUserOkToSend();
    })
}

if (userMsgContent != null) {
    userMsgContent.addEventListener("input", () => {
        checkUserOkToSend();
    })
}
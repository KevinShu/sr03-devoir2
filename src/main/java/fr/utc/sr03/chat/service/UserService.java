package fr.utc.sr03.chat.service;

import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.User;
import fr.utc.sr03.chat.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public void registerUser(User user) {
        // Générer un mot de passe aléatoire
        String randomPassword = PasswordUtil.generateRandomPassword(10);
        String hashedPassword = passwordEncoder.encode(randomPassword);
        user.setPassword(hashedPassword);
        // Sauvegarder l'utilisateur
        userRepository.save(user);
        // Envoyer un e-mail avec le mot de passe généré aléatoirement
        String subject = "Votre compte SR03 ChatApp a été créé";
        String text = "Bonjour " + user.getFirstName() + ",\n\nVotre compte a été créé avec succès. Votre mot de passe temporaire est: " + randomPassword + "\n\nVeuillez vous connecter et changer votre mot de passe dès que possible.\n\nCordialement,\n\nBingqian SHU\nHouze Zhang\nL'équipe SR03 ChatApp";
        emailService.sendEmail(user.getMail(), subject, text);
    }

    public User findByMail(String mail) {
        return userRepository.findByMail(mail);
    }
}
package fr.utc.sr03.chat.rest;

import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class RestUserController {
    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @GetMapping
    public List<String> getAllUserMails() {
        return userRepository.findAll().stream()
                .map(User::getMail)
                .collect(Collectors.toList());
    }

    @GetMapping("/{userEmail}")
    public User getUser(@PathVariable String userEmail) {
        User user = userRepository.findByMail(userEmail);
        if (user != null) {
            return user;
        } else {
            return null;
//            throw new ResourceNotFoundException("User not found with email " + userEmail);
        }
    }

    @PostMapping("/modify_info/{userEmail}")
    public User modifyUserInfo(@PathVariable String userEmail, @RequestBody ModifyUserInfoRequest modifyUserInfoRequest) {
        User user = userRepository.findByMail(userEmail);
        if (user != null) {
            user.setLastName(modifyUserInfoRequest.getLastName());
            user.setFirstName(modifyUserInfoRequest.getFirstName());
            userRepository.save(user);
            return user;
        } else {
            return null;
        }
    }

    @PostMapping("/modify_password/{userEmail}")
    public ResponseEntity<String> modifyUserPassword(@PathVariable String userEmail, @RequestBody ModifyUserPasswordRequest modifyUserPasswordRequest) {
        User user = userRepository.findByMail(userEmail);
        if (user != null) {
            if (passwordEncoder.matches(modifyUserPasswordRequest.getOldPassword(), user.getPassword())) {
                System.out.println("password match");
                // hash the password
                String hashedPassword = passwordEncoder.encode(modifyUserPasswordRequest.getPassword());
                user.setPassword(hashedPassword);
                userRepository.save(user);
                return ResponseEntity.ok(null);
            } else {
                return ResponseEntity.status(401).build();
            }
        } else {
            return ResponseEntity.status(401).build();
        }
    }

}
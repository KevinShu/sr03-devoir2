package fr.utc.sr03.chat.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.InvitationRepository;
import fr.utc.sr03.chat.dao.MessageRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.Invitation;
import fr.utc.sr03.chat.model.Message;
import fr.utc.sr03.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/conversation")
public class RestConversationController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ConversationRepository conversationRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    InvitationRepository invitationRepository;

    @PostMapping("/new")
    public ResponseEntity<String> newConversation(@RequestBody NewConversationRequest newConversationRequest) {
        if (newConversationRequest.getCreaterEmail() == null || newConversationRequest.getName() == null) {
            ResponseEntity.badRequest();
        }
        Conversation conversation = new Conversation();
        System.out.println("rest ===> new conversation, name = " + newConversationRequest.getName());
        conversation.setName(newConversationRequest.getName());
        List<User> participants = new ArrayList<User>();
        participants.add(userRepository.findByMail(newConversationRequest.getCreaterEmail()));
        conversation.setParticipants(participants);
        Conversation returnedNC = conversationRepository.save(conversation);
        if (returnedNC != null) {
            // Convert the map to a JSON string
            ObjectMapper objectMapper = new ObjectMapper();
            String json;
            try {
                json = objectMapper.writeValueAsString(returnedNC);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("Failed to convert map to JSON", e);
            }
            // Return the JSON string as the response body
            return ResponseEntity.ok(json);
        } else {
            return (ResponseEntity<String>) ResponseEntity.internalServerError();
        }
    }

    @PostMapping("/invite")
    public ResponseEntity<InvitationResponse> inviteToConversation(@RequestBody InvitationRequest invitationRequest) {
        User user_host = userRepository.findByMail(invitationRequest.getUser_host_email());
        String[] user_mail = invitationRequest.getUser_invited_email();
        List<User> user_invited = new ArrayList<>();
        Optional<Conversation> conversation = conversationRepository.findById(invitationRequest.getConversationId());
        List<String> successfullyFoundEmails = new ArrayList<>();
        List<String> notFoundEmails = new ArrayList<>();
        for (String mail : user_mail) {
            User user = userRepository.findByMail(mail);
            if (user != null) {
                user_invited.add(user);
                successfullyFoundEmails.add(mail);
            } else {
                notFoundEmails.add(mail);
            }
        }
        if (conversation.isPresent() && !user_invited.isEmpty() && user_host != null) {
            for (User user : user_invited) {
                Invitation invitation = new Invitation();
                invitation.setHostUser(user_host);
                invitation.setInvitedUser(user);
                invitation.setConversation(conversation.get());
                invitationRepository.save(invitation);
            }
            InvitationResponse invitationResponse = new InvitationResponse(successfullyFoundEmails, notFoundEmails);
            return ResponseEntity.ok(invitationResponse);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/quit")
    public ResponseEntity<String> quitConversation(@RequestBody QuitConversationRequest quitConversationRequest) {
        User utm = userRepository.findByMail(quitConversationRequest.getEmail());
        if (quitConversationRequest.getEmail() == null || quitConversationRequest.getConversationId() == 0) {
            ResponseEntity.badRequest();
        }
        Optional<Conversation> conversation = conversationRepository.findById(quitConversationRequest.getConversationId());
        if (conversation.isPresent()) {
            Conversation conv = conversation.get();
            List<Message> messages = conv.getMessages();
            List<Message> messagesToDelete = new ArrayList<>();
            for (Message message : messages) {
                if (message.getUser() == utm) {
                    messagesToDelete.add(message);
                }
            }
            for (Message message : messagesToDelete) {
                messages.remove(message);
                messageRepository.delete(message);
            }
            conv.getUsers().remove(utm);
            conversationRepository.save(conv);
            if (conv.getUsers().isEmpty()) {
                conversationRepository.delete(conv); // Delete the conversation if it has no users
            }
            return ResponseEntity.ok(null);
        } else {
            return (ResponseEntity<String>) ResponseEntity.badRequest();
        }
    }
}

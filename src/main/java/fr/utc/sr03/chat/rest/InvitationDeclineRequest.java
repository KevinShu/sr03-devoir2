package fr.utc.sr03.chat.rest;

public class InvitationDeclineRequest {
    private long invitationId;

    public InvitationDeclineRequest(long invitationId) {
        this.invitationId = invitationId;
    }

    public InvitationDeclineRequest() {
    }

    public long getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(long invitationId) {
        this.invitationId = invitationId;
    }
}
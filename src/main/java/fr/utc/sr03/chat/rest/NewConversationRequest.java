package fr.utc.sr03.chat.rest;

public class NewConversationRequest {
    private String name;

    private String createrEmail;

    public NewConversationRequest() {
    }

    public NewConversationRequest(String name, String createrEmail) {
        this.name = name;
        this.createrEmail = createrEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreaterEmail() {
        return createrEmail;
    }

    public void setCreaterEmail(String createrEmail) {
        this.createrEmail = createrEmail;
    }
}

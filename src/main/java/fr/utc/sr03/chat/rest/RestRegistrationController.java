package fr.utc.sr03.chat.rest;


import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.User;
import fr.utc.sr03.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/registration")
public class RestRegistrationController {
    @Autowired
    private UserService userService;

    private boolean isRegistrationInProgress = false;

    @PostMapping("/checkmail")
    public ResponseEntity<String> checkmail(@RequestBody EmailCheckRequest emailCheckRequest) {
        User user = userService.findByMail(emailCheckRequest.getMail());
        if (user != null) {
            return ResponseEntity.status(406).build();
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping("/new")
    public ResponseEntity<String> register(@RequestBody RegistrationRequest registrationRequest) {
        User user = userService.findByMail(registrationRequest.getMail());
        if (user != null) {
            return ResponseEntity.status(406).build();
        } else {
            if (isRegistrationInProgress) {
                return ResponseEntity.status(429).build();
            }
            isRegistrationInProgress = true;
            user = new User();
            user.setFirstName(registrationRequest.getFirstName());
            user.setLastName(registrationRequest.getLastName());
            user.setMail(registrationRequest.getMail());
            userService.registerUser(user);
            isRegistrationInProgress = false;
            return ResponseEntity.ok(null);
        }
    }
}

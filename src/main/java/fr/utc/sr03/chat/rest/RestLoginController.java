package fr.utc.sr03.chat.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.sr03.chat.model.User;
import fr.utc.sr03.chat.service.UserService;
import io.jsonwebtoken.io.Decoders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class RestLoginController {

    @Value("${jwt.secret-key}")
    private String secretKey;
    @Autowired
    private UserService userService;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginRequest loginRequest) {
        User user = userService.findByMail(loginRequest.getMail());
        if (user != null) {
            boolean isPasswordMatch = passwordEncoder.matches(loginRequest.getPassword(), user.getPassword());
            if (isPasswordMatch) {
                byte[] keyBytes = Decoders.BASE64.decode(secretKey);
                Key key = Keys.hmacShaKeyFor(keyBytes);
                String jws = Jwts.builder()
                        .setSubject(user.getMail())
//                        .setExpiration(new Date(System.currentTimeMillis() + 10)) // Set expiration to 1 hour
                        .setExpiration(new Date(System.currentTimeMillis() + 3600000)) // Set expiration to 1 hour
                        .signWith(key)
                        .compact();

                // Create a map with a single key-value pair
                Map<String, String> map = new HashMap<>();
                map.put("token", jws);

                // Convert the map to a JSON string
                ObjectMapper objectMapper = new ObjectMapper();
                String json;
                try {
                    json = objectMapper.writeValueAsString(map);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException("Failed to convert map to JSON", e);
                }

                // Return the JSON string as the response body
                return ResponseEntity.ok(json);
            }
        }
        return ResponseEntity.status(401).build();
    }
}
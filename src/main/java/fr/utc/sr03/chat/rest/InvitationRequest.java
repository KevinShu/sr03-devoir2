package fr.utc.sr03.chat.rest;

public class InvitationRequest {

    private String user_host_email;
    private String[] user_invited_email;
    private long conversationId;

    public InvitationRequest() {
    }

    public InvitationRequest(String user_host_email, String[] user_invited_email, long conversationId) {
        this.user_host_email = user_host_email;
        this.user_invited_email = user_invited_email;
        this.conversationId = conversationId;
    }

    public String getUser_host_email() {
        return user_host_email;
    }

    public void setUser_host_email(String user_host_email) {
        this.user_host_email = user_host_email;
    }

    public String[] getUser_invited_email() {
        return user_invited_email;
    }

    public void setUser_invited_email(String[] user_invited_email) {
        this.user_invited_email = user_invited_email;
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }
}

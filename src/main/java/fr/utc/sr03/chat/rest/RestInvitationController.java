package fr.utc.sr03.chat.rest;

import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.InvitationRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.Invitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.User;
import fr.utc.sr03.chat.model.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/invitation")
public class RestInvitationController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private InvitationRepository invitationRepository;

    @GetMapping("/{userEmail}")
    public List<Invitation> getInvitations(@PathVariable String userEmail) {
        User user = userRepository.findByMail(userEmail);
        List<Invitation> invitation = invitationRepository.findAllByInvitedUser(user);
        if (!invitation.isEmpty()) {
            return new ArrayList<>(invitation);
        } else {
            System.out.println("no notification found for " + userEmail);
            return new ArrayList<>();
        }
    }

    @PostMapping("/accept")
    public ResponseEntity<String> acceptInvitation(@RequestBody InvitationAcceptRequest invitationAcceptRequest) {
        Optional<Invitation> inv = invitationRepository.findById(invitationAcceptRequest.getInvitationId());
        if (inv.isPresent()) {
            Invitation invitation = inv.get();
            User invitedUser = invitation.getInvitedUser();
            Conversation conversation = invitation.getConversation();
            // Add the invited user to the conversation
            conversation.getUsers().add(invitedUser);
            conversationRepository.save(conversation);
            // Delete the invitation
            invitationRepository.delete(invitation);
            return ResponseEntity.ok(null);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/decline")
    public ResponseEntity<String> declineInvitation(@RequestBody InvitationDeclineRequest invitationDeclineRequest) {
        Optional<Invitation> inv = invitationRepository.findById(invitationDeclineRequest.getInvitationId());
        if (inv.isPresent()) {
            Invitation invitation = inv.get();
            // Delete the invitation
            invitationRepository.delete(invitation);
            return ResponseEntity.ok(null);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}

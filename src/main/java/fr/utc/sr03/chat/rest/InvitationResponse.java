package fr.utc.sr03.chat.rest;

import java.util.List;

public class InvitationResponse {
    private List<String> success;
    private List<String> failed;

    public InvitationResponse(List<String> success, List<String> failed) {
        this.success = success;
        this.failed = failed;
    }

    public List<String> getSuccess() {
        return success;
    }

    public List<String> getFailed() {
        return failed;
    }
}
package fr.utc.sr03.chat.rest;

public class EmailCheckRequest {
    private String mail;

    public EmailCheckRequest() {
    }

    public EmailCheckRequest(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}

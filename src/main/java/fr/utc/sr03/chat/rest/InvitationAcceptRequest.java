package fr.utc.sr03.chat.rest;

public class InvitationAcceptRequest {
    private long invitationId;

    public InvitationAcceptRequest(long invitationId) {
        this.invitationId = invitationId;
    }

    public InvitationAcceptRequest() {
    }

    public long getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(long invitationId) {
        this.invitationId = invitationId;
    }
}

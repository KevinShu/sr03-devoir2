package fr.utc.sr03.chat.rest;

public class QuitConversationRequest {
    private long conversationId;

    private String email;

    public QuitConversationRequest() {
    }

    public QuitConversationRequest(long conversationId, String email) {
        this.conversationId = conversationId;
        this.email = email;
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

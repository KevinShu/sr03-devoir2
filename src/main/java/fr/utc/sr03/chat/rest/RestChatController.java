package fr.utc.sr03.chat.rest;

import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.User;
import fr.utc.sr03.chat.model.Message;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/chat")
public class RestChatController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @GetMapping("/{userEmail}")
    public List<Conversation> getConversations(@PathVariable String userEmail) {
        User user = userRepository.findByMail(userEmail);
        if (user != null) {
            return user.getConversations().stream().map(conversation -> {
                conversation.getMessages().sort((message1, message2) -> message1.getDate().compareTo(message2.getDate()));
                return conversation;
            }).collect(Collectors.toList());
        } else {
            System.out.println("user not found");
            return null;
//            throw new ResourceNotFoundException("User not found with email " + userEmail);
        }
    }
}
package fr.utc.sr03.chat.rest;

public class ModifyUserPasswordRequest {
    String password;

    String oldPassword;

    public ModifyUserPasswordRequest() {
    }

    public ModifyUserPasswordRequest(String password, String oldPassword) {
        this.password = password;
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}

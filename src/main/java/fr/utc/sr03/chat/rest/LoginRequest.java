package fr.utc.sr03.chat.rest;

public class LoginRequest {
    private String mail;
    private String password;

    // getters and setters

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

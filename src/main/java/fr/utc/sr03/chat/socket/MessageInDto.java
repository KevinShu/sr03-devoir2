package fr.utc.sr03.chat.socket;

import java.util.Date;

public class MessageInDto {
    private String sender;
    private String senderMail;
    private String content;
    private Long conversationId;
    private Date date;

    public MessageInDto() {
    }

    public MessageInDto(String sender, String senderMail, String content, Long conversationId, Date date) {
        this.sender = sender;
        this.senderMail = senderMail;
        this.content = content;
        this.conversationId = conversationId;
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderMail() {
        return senderMail;
    }

    public void setSenderMail(String senderMail) {
        this.senderMail = senderMail;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

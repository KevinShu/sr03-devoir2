package fr.utc.sr03.chat.socket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.User;
import fr.utc.sr03.chat.model.Message;
import fr.utc.sr03.chat.dao.MessageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    // Create an instance of ObjectMapper
    ObjectMapper objectMapper = new ObjectMapper();

    @MessageMapping("/chat")
    public void sendMessage(MessageInDto messageInDto) throws JsonProcessingException {
        // Extract the conversation ID from the payload.
        Long conversationId = messageInDto.getConversationId();
        List<User> users = null;
        // Look up the users to broadcast to based on the conversation ID.
        /*
         * When we retrieve the Conversation entity from the repository using conversationRepository.findById(conversationId),
         * Hibernate fetches the Conversation entity but does not load its associated users collection immediately.
         * Instead, it initializes the collection lazily, meaning it will be loaded when accessed within an active Hibernate session.
         *
         * Solution:
         * Eagerly fetch the users collection along with the Conversation entity by modifying the repository query.
         * This way, the users collection will be loaded immediately and available outside the session.
         * */
        Optional<Conversation> conversation = conversationRepository.findByIdWithUsers(conversationId);
        User user = userRepository.findByMail(messageInDto.getSenderMail());
        // message to save in database
        Message message = null;
        if (conversation.isPresent() && user != null) {
            users = conversation.get().getUsers();
            // Create a new message instance and set its properties
            message = new Message();
            message.setContent(messageInDto.getContent());
            message.setUser(user);
            message.setConversation(conversation.get());
            // Convert the Date to Instant
            Instant instant = messageInDto.getDate().toInstant();
            // Create LocalDateTime from Instant
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            message.setDate(localDateTime);
            // Save the message to the database
            messageRepository.save(message);
        }
        // Broadcast the message to all users in the conversation.
        for (User destinataire : users) {
            System.out.println("sending message to: /user/" + destinataire.getMail() + "/queue/messages");
            messagingTemplate.convertAndSendToUser(destinataire.getMail(), "/queue/messages", message);
        }
    }
}
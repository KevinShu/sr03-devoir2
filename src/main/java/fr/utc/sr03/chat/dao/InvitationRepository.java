package fr.utc.sr03.chat.dao;

import fr.utc.sr03.chat.model.Invitation;
import fr.utc.sr03.chat.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation, Long> {
    List<Invitation> findAllByInvitedUser(User invitedUser);
}
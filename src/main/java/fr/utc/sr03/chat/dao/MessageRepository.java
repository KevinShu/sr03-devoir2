package fr.utc.sr03.chat.dao;

import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.Message;
import fr.utc.sr03.chat.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByUserId(long userId);

    List<Message> findAllByConversationId(long conversationId);

    Message findByDate(String date);
}

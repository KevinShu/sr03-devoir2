package fr.utc.sr03.chat.dao;

import fr.utc.sr03.chat.model.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ConversationRepository extends JpaRepository<Conversation, Long> {
    Conversation findByName(String name);

    // eagerly fetch users
    @Query("SELECT DISTINCT c FROM Conversation c JOIN FETCH c.users WHERE c.id = :conversationId")
    Optional<Conversation> findByIdWithUsers(@Param("conversationId") Long conversationId);

    @Query("SELECT c FROM Conversation c JOIN c.users u WHERE u.id = :userId")
    List<Conversation> findAllByUserId(@Param("userId") Long userId);

}

package fr.utc.sr03.chat.config;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy // enable Spring AOP
public class AopConfig {
}

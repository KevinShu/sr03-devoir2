package fr.utc.sr03.chat.config;

import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration // tell the Spring container that this class is a configuration class
@ComponentScan(basePackages = {"fr.utc.sr03.chat.aop", "fr.utc.sr03.chat.test"}) // scan all components
@Import(fr.utc.sr03.chat.config.AopConfig.class) // import AopConfig
public class BaseConfig {
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurer() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**")
//                        .allowedOrigins("http://localhost:3000")
//                        .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS")
//                        .allowedHeaders("*");
//            }
//        };
//    }
}

package fr.utc.sr03.chat;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
@SessionAttributes("currentUser")
public class CustomErrorController implements ErrorController {
    private static final String ERROR_PATH = "/error";


    @RequestMapping(ERROR_PATH)
    public String handleError(HttpServletRequest request, Model model) {

        // Get the error status code and message from the request
        Integer status = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
//        Object message = request.getAttribute(RequestDispatcher.ERROR_MESSAGE);

        if (status != null) {
            // Add the error status code and message to the model
            model.addAttribute("status", status);
            System.out.println(status);
            // set the error message in the model
            String message = getErrorMessage(status);
            if (message != null) {
                model.addAttribute("message", message);
            }
        }

        // Return the path to the error view
        return "error";
    }

    private String getErrorMessage(int statusCode) {
        switch (statusCode) {
            case 400:
                return "Bad Request";
            case 401:
            case 403:
                return "Unauthorized";
            case 404:
                return "Not Found";
            case 500:
                return "Internal Server Error";
            default:
                return "Unknown Error";
        }
    }

    public String getErrorPath() {
        return ERROR_PATH;
    }

}

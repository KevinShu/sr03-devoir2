package fr.utc.sr03.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    private JwtTokenFilter jwtTokenFilter;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOriginPatterns(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()   // cors filter should be in front of other filters
                .csrf().disable() //temporarily disable csrf functionnality for testing at client side, re-enable when deployment
                .authorizeRequests()
                .antMatchers("/admin/**").access("hasAuthority('ROLE_ADMIN')") // Allow only users with the ROLE_ADMIN authority to access URLs starting with /admin
                .antMatchers("/home").access("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
                .antMatchers("/conversations").access("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
                .antMatchers("/conversations/new").access("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
                .antMatchers("/messages").access("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
                .antMatchers("/login").permitAll()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/registration").permitAll()
                .antMatchers("/api/registration/checkmail").permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/error");

        // Apply JWT
        http.antMatcher("/api/**").addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/js/loginFormValidation.js");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(new CustomAuthenticationProvider());
    }

    private class CustomAuthenticationProvider implements AuthenticationProvider {
        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            // Get the user attribute from the session
            // fr.utc.sr03.chat.model.User currentUser = (fr.utc.sr03.chat.model.User) authentication.getDetails();
            // Create a custom UserDetails object based on the authentication data
            UserDetails userDetails = User.builder()
                    .username(authentication.getName())
                    .password(authentication.getCredentials().toString())
                    .authorities(authentication.getAuthorities())
                    .build();

            return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        }

        @Override
        public boolean supports(Class<?> authentication) {
            return authentication.equals(UsernamePasswordAuthenticationToken.class);
        }
    }
}
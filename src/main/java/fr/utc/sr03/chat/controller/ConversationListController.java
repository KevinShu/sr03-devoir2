package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("currentUser")
@RequestMapping("conversations")
public class ConversationListController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConversationRepository conversationRepository;

    @GetMapping
    public String getConversation(@ModelAttribute("currentUser") User currentUser, Model model) {
        System.out.println("enters get Conversation");
        List<Conversation> conversations = conversationRepository.findAllByUserId(currentUser.getId());
        model.addAttribute("conversations", conversations);
        return "conversation_list";
    }

    @GetMapping("new")
    public String getNewConversation(@ModelAttribute("currentUser") User currentUser, Model model) {
        model.addAttribute("conversation", new Conversation());
        return "conversation_form";
    }

    @PostMapping("new")
    public String newConversation(@ModelAttribute Conversation conversation, @ModelAttribute("currentUser") User currentUser, Model model) {
        System.out.println("backend admin ===> new conversation, name = " + conversation.getName());
        conversation.setName(conversation.getName());
        List<User> participants = new ArrayList<User>();
        participants.add(currentUser);
        conversation.setParticipants(participants);
        Conversation returnedNC = conversationRepository.save(conversation);
        if (returnedNC != null) {
            model.addAttribute("alertClassName", "alert alert-success");
            model.addAttribute("alertText", "Conversation " + returnedNC.getName() + " successfully added!");
        } else {
            model.addAttribute("alertClassName", "alert alert-danger");
            model.addAttribute("alertText", "Something went wrong, try again later.");
        }
        return "conversation_form";
    }
}

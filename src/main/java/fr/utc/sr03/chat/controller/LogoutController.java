package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@SessionAttributes("currentUser")
@RequestMapping("logout")
public class LogoutController {
    @GetMapping
    public String getLogout(HttpSession session, SessionStatus status) {

        // remove the username from the session
        session.removeAttribute("currentUser");
        status.setComplete();

        // Clear the security context
        SecurityContextHolder.getContext().setAuthentication(null);

        return "redirect:/login";
    }
}

package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import fr.utc.sr03.chat.model.User;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    private boolean isRegistrationInProgress = false;

    @GetMapping
    public String showRegistrationForm(Model model) {
        model.addAttribute("user", new User());
        return "registration_form";
    }

    @PostMapping
    public String registerUser(@ModelAttribute User user) {
        if (userRepository.findByMail(user.getMail()) != null || isRegistrationInProgress) {
            return "registration_failed";
        }
        isRegistrationInProgress = true;
        userService.registerUser(user);
        isRegistrationInProgress = false;
        return "registration_success";
    }
}

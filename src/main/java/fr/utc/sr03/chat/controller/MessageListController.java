package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.MessageRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.Message;
import fr.utc.sr03.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@SessionAttributes("currentUser")
@RequestMapping("messages")
public class MessageListController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private MessageRepository messageRepository;

    @GetMapping
    public String getMessage(@ModelAttribute("currentUser") User currentUser, Model model) {
        List<Message> messagesSent = new ArrayList<Message>();
        List<Message> messagesReceived = new ArrayList<Message>();
        messagesSent = messageRepository.findAllByUserId(currentUser.getId());
        List<Conversation> conversationParticipated;

        // find all conversations that the current user take part in
        conversationParticipated = conversationRepository.findAllByUserId(currentUser.getId());
        // for each conversation founded, get all messages in it
        for (Conversation conversation : conversationParticipated) {
            messagesReceived.addAll(messageRepository.findAllByConversationId(conversation.getId()));
        }
        model.addAttribute("messagesSent", messagesSent);
        model.addAttribute("messagesReceived", messagesReceived);
        return "message_list";
    }

    @GetMapping("new")
    public String getNewMessage(@ModelAttribute("currentUser") User currentUser, Model model) {
        model.addAttribute("newMessage", new Message());
        model.addAttribute("conversations", conversationRepository.findAllByUserId(currentUser.getId()));
        return "message_form";
    }

    @PostMapping("new")
    public String newMessage(@ModelAttribute Message newMessage, @ModelAttribute("currentUser") User currentUser, Model model) {
        // Find the user in the database (not possible to use currentUser as attribute directly)
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            newMessage.setUser(user);
        } else {
            // Handle the case when the user is not found in the database
            model.addAttribute("alertClassName", "alert alert-danger");
            model.addAttribute("alertText", "User not found.");
            model.addAttribute("newMessage", newMessage);
            return "message_form";
        }
        newMessage.setContent(newMessage.getContent());
        newMessage.setDate(newMessage.getDate());
        Message returnedNM = messageRepository.save(newMessage);
        if (returnedNM != null) {
            model.addAttribute("alertClassName", "alert alert-success");
            model.addAttribute("alertText", "Message " + returnedNM.getId() + " : " + returnedNM.getContent() + " has been sent to conversation " + returnedNM.getConversation().getId() + " successfully!");
        } else {
            model.addAttribute("alertClassName", "alert alert-danger");
            model.addAttribute("alertText", "Something went wrong, try again later.");
        }
        model.addAttribute("newMessage", newMessage);
        return "message_form";
    }
}

package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes("currentUser")
@RequestMapping("login")
public class LoginController {
    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private AuthenticationManager authenticationManager;

    @GetMapping
    public String getLogin(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @PostMapping
    public String postLogin(@ModelAttribute User user, Model model) {
        // Query the user repository to check if the email and password match
        User userBDD = userRepository.findByMail(user.getMail());
        if (userBDD != null) {
            boolean isPasswordMatch = passwordEncoder.matches(user.getPassword(), userBDD.getPassword());
            if (isPasswordMatch) {
                // If the user exists, add the user to the model and return the success view
                model.addAttribute("currentUser", userBDD);
                if (userBDD.isAdmin()) {

                    List<User> users = userRepository.findAll();
                    model.addAttribute("users", users);

                    List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
                    grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

                    // Create an authentication token with the user details and user attributes
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userBDD.getMail(), userBDD.getPassword(), grantedAuths);
//                authentication.setDetails(userBDD);

                    // Authenticate the user and set the authentication token in the security context
                    Authentication result = authenticationManager.authenticate(authentication);
                    SecurityContextHolder.getContext().setAuthentication(result);

                    // Redirect to the home page
                } else {
                    List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
                    grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
                    // Create an authentication token with the user details and user attributes
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getMail(), user.getPassword(), grantedAuths);
                    // Authenticate the user and set the authentication token in the security context
                    Authentication result = authenticationManager.authenticate(authentication);
                    SecurityContextHolder.getContext().setAuthentication(result);
                    // Redirect to the home page
                }
                return "home";
            } else {
                // If the user does not exist, add an error message to the model and return the login view
                model.addAttribute("error", "Invalid email or password");
                return "login";
            }
        } else {
            // If the user does not exist, add an error message to the model and return the login view
            model.addAttribute("error", "Invalid email or password");
            return "login";
        }
    }
}

package fr.utc.sr03.chat.controller;

import fr.utc.sr03.chat.dao.ConversationRepository;
import fr.utc.sr03.chat.dao.MessageRepository;
import fr.utc.sr03.chat.dao.UserRepository;
import fr.utc.sr03.chat.model.Conversation;
import fr.utc.sr03.chat.model.Message;
import fr.utc.sr03.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@SessionAttributes("currentUser")
@RequestMapping("admin")
public class AdminController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConversationRepository conversationRepository;
    @Autowired
    private MessageRepository messageRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Autowired
    private HttpServletRequest request;

    @GetMapping("users")
    public String getUserList(Model model) {
        List<User> users = userRepository.findAll();
        model.addAttribute("users", users);
        return "user_list";
    }

    @GetMapping("conversations")
    public String getConversationList(Model model) {
        List<Conversation> conversations = conversationRepository.findAll();
        model.addAttribute("conversations", conversations);
        return "conversation_list";
    }

    @GetMapping("messages")
    public String getMessage(@ModelAttribute("currentUser") User currentUser, Model model) {
        List<Message> messagesSent = new ArrayList<Message>();
        List<Message> messagesReceived = new ArrayList<Message>();
        List<Message> adminAllMessages = new ArrayList<Message>();
        if (currentUser.isAdmin()) {
            adminAllMessages = messageRepository.findAll();
        }
        messagesSent = messageRepository.findAllByUserId(currentUser.getId());
        List<Conversation> conversationParticipated;

        // find all conversations that the current user take part in
        conversationParticipated = conversationRepository.findAllByUserId(currentUser.getId());
        // for each conversation founded, get all messages in it
        for (Conversation conversation : conversationParticipated) {
            messagesReceived.addAll(messageRepository.findAllByConversationId(conversation.getId()));
        }
        model.addAttribute("messagesSent", messagesSent);
        model.addAttribute("messagesReceived", messagesReceived);
        model.addAttribute("adminAllMessages", adminAllMessages);
        return "message_list";
    }

    @GetMapping("status")
    public String getStatus(@ModelAttribute("currentUser") User currentUser, Model model) {
        return "status";
    }

    @GetMapping("messages/new")
    public String getNewMessage(@ModelAttribute("currentUser") User currentUser, Model model) {
        model.addAttribute("newMessage", new Message());
        model.addAttribute("conversations", conversationRepository.findAll());
        return "message_form";
    }

    @PostMapping("messages/new")
    public String newMessage(@ModelAttribute Message newMessage, @ModelAttribute("currentUser") User currentUser, Model model) {
        // Find the user in the database
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            newMessage.setUser(user);
        } else {
            // Handle the case when the user is not found in the database
            model.addAttribute("alertClassName", "alert alert-danger");
            model.addAttribute("alertText", "User not found.");
            model.addAttribute("newMessage", newMessage);
            return "message_form";
        }
        newMessage.setContent(newMessage.getContent());
        newMessage.setDate(newMessage.getDate());
        Message returnedNM = messageRepository.save(newMessage);
        if (returnedNM != null) {
            model.addAttribute("alertClassName", "alert alert-success");
            model.addAttribute("alertText", "Message " + returnedNM.getId() + " : " + returnedNM.getContent() + " has been sent to conversation " + returnedNM.getConversation().getId() + " successfully!");
        } else {
            model.addAttribute("alertClassName", "alert alert-danger");
            model.addAttribute("alertText", "Something went wrong, try again later.");
        }
        model.addAttribute("newMessage", newMessage);
        return "message_form";
    }

    @GetMapping("users/add")
    public String getUserForm(Model model) {
        model.addAttribute("user", new User());
        return "user_form";
    }

    @GetMapping("users/modify")
    public String getUserFormModify(@RequestParam("userid") Long userId, Model model) {
        User utm = userRepository.findById(userId).orElse(null);
        if (utm != null) {
            model.addAttribute("userToModify", utm);
            return "user_form_modify";
        } else {
            return "error";
        }
    }

    @GetMapping("users/delete")
    public String getUserFormDelete(@RequestParam("userid") Long userId, Model model) {
        User utm = userRepository.findById(userId).orElse(null);
        if (utm != null) {
            List<Conversation> conversations = utm.getConversations();
            for (Conversation conversation : conversations) {
                List<Message> messages = conversation.getMessages();
                List<Message> messagesToDelete = new ArrayList<>();
                for (Message message : messages) {
                    if (message.getUser() == utm)
                        messagesToDelete.add(message);
                }
                for (Message message : messagesToDelete) {
                    messages.remove(message);
                    messageRepository.delete(message);
                }
                conversation.getUsers().remove(utm);
                conversationRepository.save(conversation);
            }
            userRepository.deleteById(userId);
            List<User> users = userRepository.findAll();
            model.addAttribute("users", users);
            model.addAttribute("alertClassName", "alert alert-warning");
            model.addAttribute("alertText", "User " + utm.getUsernameLine() + " has been deleted!");
            return "user_list";
        } else {
            return "error";
        }
    }

    @PostMapping("users/add")
    public String addUser(@ModelAttribute User user, Model model) {
        System.out.println("===> first name = " + user.getFirstName());
        System.out.println("===> last name = " + user.getLastName());
        user.setMail(user.getMail());
        // hash the password
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setAdmin(user.isAdmin());
        User returnedNU = userRepository.save(user);
        if (returnedNU != null) {
            model.addAttribute("alertClassName", "alert alert-success");
            model.addAttribute("alertText", "User " + returnedNU.getUsernameLine() + " successfully added!");
        } else {
            model.addAttribute("alertClassName", "alert alert-danger");
            model.addAttribute("alertText", "Something went wrong, try again later.");
        }
        return "user_form";
    }

    @PostMapping("users/modify")
    public String modifyUser(@RequestParam("userid") Long userId, @ModelAttribute("userToModify") User userToModify, Model model) throws ServletException {
        // Get user from the database
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            // Update the user's name and email fields
            User updateUser = userOptional.get();
            updateUser.setFirstName(userToModify.getFirstName());
            updateUser.setLastName(userToModify.getLastName());
            updateUser.setMail(userToModify.getMail());
            updateUser.setAdmin(userToModify.isAdmin());
            String hashedPassword = passwordEncoder.encode(userToModify.getPassword());
            updateUser.setPassword(hashedPassword);
            // Save the updated user object in the database
            userRepository.save(updateUser);
            List<User> users = userRepository.findAll();
            model.addAttribute("users", users);
            model.addAttribute("alertClassName", "alert alert-success");
            model.addAttribute("alertText", "Information of user " + updateUser.getUsernameLine() + " successfully updated!");
            return "user_list";
        } else {
            return "error";
        }
    }
}

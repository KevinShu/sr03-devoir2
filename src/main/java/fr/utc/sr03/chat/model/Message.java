package fr.utc.sr03.chat.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

@Entity
@Table(name = "sr03_messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonBackReference(value = "user-message") // Make Message the back reference of the relationship
    private User user;

    @ManyToOne
    @JoinColumn(name = "conversation_id")
    @JsonBackReference(value = "conversation-message") // Make Message the back reference of the relationship
    private Conversation conversation;

    @Lob
    @Column(name = "content", columnDefinition = "LONGBLOB")
    private byte[] content;

    @Column(name = "date")
    private LocalDateTime date;

    public Message() {
        this.conversation = new Conversation();
        this.user = new User();
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getSender() {
        if (user != null) {
            return user.getUsernameLine();
        }
        return null;
    }

    public String getSenderMail() {
        if (user != null) {
            return user.getMail();
        }
        return null;
    }

    public long getConversationIdNumber() {
        if (conversation != null) {
            return conversation.getId();
        }
        return 0;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public String getContent() {
        return new String(content, StandardCharsets.UTF_8);
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public void setContent(String str) {
        content = str.getBytes(StandardCharsets.UTF_8);
    }

}

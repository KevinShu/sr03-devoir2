package fr.utc.sr03.chat.model;

import java.util.List;

public class ConversationManagement {
    public void createConversation(Conversation conversation) {
        // Create the conversation in the database
    }

    public void addParticipant(int conversationId, int userId) {
        // Add a participant to the conversation in the database
    }

    public void removeParticipant(int conversationId, int userId) {
        // Remove a participant from the conversation in the database
    }

    public void addMessage(int conversationId, Message message) {
        // Add the message to the conversation in the database
    }

    public List<Conversation> searchUserConversations(int userId) {
        // Search for all conversations in which the user participates in the database
        // Return a list of conversations
        return null;
    }

    public List<Message> searchConversationMessages(int conversationId) {
        // Search for all messages in a conversation in the database
        // Return a list of messages
        return null;
    }
}

package fr.utc.sr03.chat.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sr03_users")
public class User {
    @Id
    // 这个ID是系统自动生成的，不需要我们手动设置
    @GeneratedValue(strategy=GenerationType.IDENTITY) // strategy=GenerationType.IDENTITY => obligatoire pour auto increment mysql
    private long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "mail")
    private String mail;

    @Column(name = "psw")
    @JsonIgnore
    private String password;

    @Column(name = "is_admin")
    private boolean admin;

    // 一个用户可以参与多个会话 => 多对多

    @ManyToMany(mappedBy = "users")
    @JsonIgnore
    private List<Conversation> conversations = new ArrayList<>();


    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getUsernameLine() {
        return getFirstName() + " " + getLastName() + " (" + getMail() + ")";
    }

    public List<Conversation> getConversations() {
        return this.conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }
}

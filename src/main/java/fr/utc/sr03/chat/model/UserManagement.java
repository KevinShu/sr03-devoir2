package fr.utc.sr03.chat.model;

public class UserManagement {
    public void createUser(User user) {
        // Create the user in the database
    }

    public void updateUser(User user) {
        // Update the user in the database
    }

    public void deleteUser(int id) {
        // Delete the user from the database
    }

    public User authenticate(String email, String password) {
        // Search for the user in the database based on email and password
        // Return the user if found, or null otherwise
        return null;
    }
}

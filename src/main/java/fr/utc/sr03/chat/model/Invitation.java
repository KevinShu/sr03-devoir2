package fr.utc.sr03.chat.model;

import javax.persistence.*;

@Entity
@Table(name = "sr03_invitations")
public class Invitation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_host_id")
    private User hostUser;

    @ManyToOne
    @JoinColumn(name = "user_invited_id")
    private User invitedUser;

    @ManyToOne
    @JoinColumn(name = "conversation_id")
    private Conversation conversation;

    @Column(name = "confirmed")
    private boolean confirmed;

    public Invitation() {
    }

    public Invitation(Long id, User hostUser, User invitedUser, Conversation conversation, boolean confirmed) {
        this.id = id;
        this.hostUser = hostUser;
        this.invitedUser = invitedUser;
        this.conversation = conversation;
        this.confirmed = confirmed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getHostUser() {
        return hostUser;
    }

    public void setHostUser(User hostUser) {
        this.hostUser = hostUser;
    }

    public User getInvitedUser() {
        return invitedUser;
    }

    public void setInvitedUser(User invitedUser) {
        this.invitedUser = invitedUser;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }
}
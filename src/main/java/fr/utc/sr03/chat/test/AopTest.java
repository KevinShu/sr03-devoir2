package fr.utc.sr03.chat.test;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class AopTest {
    public static void main(String[] args) {
        System.out.println("main");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(fr.utc.sr03.chat.config.BaseConfig.class);
        ctx.getBean(AopTest.class).Test1();
        ctx.getBean(AopTest.class).Test2();
    }
    @Test
    void Test1() {
        System.out.println("Test1");
    }

    @Test
    void Test2() {
        System.out.println("Test2");
    }
}

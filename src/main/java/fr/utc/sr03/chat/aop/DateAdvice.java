package fr.utc.sr03.chat.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Component;

@Component
// tell the Spring container that this class is an aspect
@Aspect
public class DateAdvice {
    // pointcut, private method
    @Pointcut("execution(void fr.utc.sr03.chat.test.AopTest.Test1())")
    private void pt(){}

    @Before("pt()")
    // advice method
    public void method() {
        System.out.println(System.currentTimeMillis());
    }
}

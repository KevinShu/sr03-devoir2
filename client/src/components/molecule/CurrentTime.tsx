import React, {useEffect, useState} from 'react';

const CurrentTime: React.FC = () => {
    const [currentTime, setCurrentTime] = useState(new Date());

    useEffect(() => {
        const timer = setInterval(() => {
            setCurrentTime(new Date());
        }, 1000); // Update time every 1 second

        // Cleanup function to clear the interval when component unmounts
        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <div>
            {currentTime.toLocaleTimeString()}
        </div>
    );
};

const CurrentDateTime: React.FC = () => {
    const [currentDateTime, setCurrentDateTime] = useState(new Date());

    useEffect(() => {
        const timer = setInterval(() => {
            setCurrentDateTime(new Date());
        }, 1000); // Update time every 1 second

        // Cleanup function to clear the interval when component unmounts
        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
        <div>
            {currentDateTime.toLocaleString()}
        </div>
    );
};

export {CurrentDateTime, CurrentTime};
import React, {useState, useEffect} from 'react'
import Typography from '@mui/material/Typography'
import Breadcrumbs from '@mui/material/Breadcrumbs'
import Link from '@mui/material/Link'
import {useNavigate, useLocation} from 'react-router-dom'
import {Box} from '@mui/material'
import HomeIcon from '@mui/icons-material/Home'
import {APP_ROUTES} from 'router/Routes'

export default function CustomBreadcrumbs() {
    const navigate = useNavigate()
    const location = useLocation()
    const [pageNames, setPageNames] = useState<string[]>([])
    useEffect(() => {
        const path = location.pathname
        const pages = path.split('/')
        setPageNames(pages)
    }, [location])
    return (
        <Box display="flex">
            {location.pathname !== APP_ROUTES.LOGIN &&
                <HomeIcon fontSize="medium" sx={{mt: 0.1, color: "white", transform: "scale(0.7)"}}
                          onClick={() => navigate(APP_ROUTES.ROOT)}/>}
            <Breadcrumbs separator="›" sx={{ml: -0.7, color: "white"}} aria-label="breadcrumb">
                {pageNames.map((pagename, index) => {
                    const subPageNames = pageNames.slice(0, index + 1)
                    const linkpath = subPageNames.join('/')
                    return index === pageNames.length - 1 ? (
                        <Typography
                            key={`breadcrumbs-typography-${pagename}`}
                            noWrap
                            fontSize="small"
                            sx={{mt: '2px'}}
                        >
                            {pageNames[pageNames.length - 1]}
                        </Typography>
                    ) : (
                        <Link
                            key={`breadcrumbs-link-${pagename}`}
                            noWrap
                            color="#fff"
                            fontSize="small"
                            underline="always"
                            href={linkpath}
                        >
                            {pagename}
                        </Link>
                    )
                })}
            </Breadcrumbs>
        </Box>
    )
}

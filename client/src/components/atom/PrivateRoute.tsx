import React from 'react'
import {Navigate, useLocation} from 'react-router-dom'

const PrivateRoute: React.FC<{
    children: JSX.Element
}> = ({children}) => {
    const client = localStorage.getItem('user')
    const authenticated = !!localStorage.getItem('token');
    const location = useLocation()

    if (authenticated !== null) {
        if (authenticated) {
            return children
        }
        return (
            <Navigate
                to={`/login?returnUrl=${encodeURIComponent(
                    location.pathname + location.search
                )}`}
            />
        )
    }

    return null
}

export default PrivateRoute

import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {Box} from "@mui/system";

type ChildComponentProps = {
    currentConversationId: any;
    setCurrentConversationId: React.Dispatch<React.SetStateAction<number | null>>;
    chat: any;
    setChat: React.Dispatch<React.SetStateAction<any>>;
    scrollConversationListToBottom: () => void;
};

export default function QuitConversationDialog(props: ChildComponentProps) {
    const {chat, currentConversationId} = props;
    const [open, setOpen] = React.useState(false);
    const [quitSuccessDialogOpen, setQuitSuccessDialogOpen] = React.useState(false);
    const [quitFailedDialogOpen, setQuitFailedDialogOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleQuitSuccessDialogClose = () => {
        setQuitSuccessDialogOpen(false);
        window.location.reload();
    };

    const handleQuitFailedDialogClose = () => {
        setQuitFailedDialogOpen(false);
    };

    const handleConfirmQuit = async () => {
        setOpen(false);
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/conversation/quit`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify({
                email: localStorage.getItem('email'),
                conversationId: currentConversationId,
            }),
        });

        if (response.ok) {
            setQuitSuccessDialogOpen(true);
            console.log("Quit from conversation");
        } else {
            setQuitFailedDialogOpen(true);
            console.error('Quit failed');
        }
    };


    return (
        <Box sx={{padding: '10px'}}>
            <Button variant="outlined" color="error" onClick={handleClickOpen} sx={{width: '100%'}}>
                Quit conversation
            </Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Quit conversation</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Vous êtes en train de quitter la conversation {chat.map((conv: any) => {
                        if (conv.id === currentConversationId)
                            return conv.name;
                        else
                            return null;
                    })}. Tous les messages envoyés par vous seront effacés, vous allez perdu également l'histoire de
                        chat. Êtes-vous sûr ?
                    </DialogContentText>
                    <br/>
                    <br/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Annuler</Button>
                    <Button onClick={() => {
                        handleConfirmQuit();
                    }}>Oui</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={quitSuccessDialogOpen} onClose={handleQuitSuccessDialogClose}>
                <DialogTitle>Quit success</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Vous avez quitté cette conversation.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleQuitSuccessDialogClose}>OK</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={quitFailedDialogOpen} onClose={handleQuitFailedDialogClose}>
                <DialogTitle>Quit failed</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Il y a un problem. Veuillez réssayer plus tard.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleQuitFailedDialogClose}>OK</Button>
                </DialogActions>
            </Dialog>
        </Box>
    );
}
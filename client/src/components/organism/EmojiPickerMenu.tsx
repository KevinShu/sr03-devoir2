import * as React from 'react';
import Menu from '@mui/material/Menu';
import InsertEmoticonIcon from '@mui/icons-material/InsertEmoticon';
import EmojiPicker from "emoji-picker-react";
import {IconButton} from "@mui/material";
import {EmojiClickData} from "emoji-picker-react/src/types/exposedTypes";

type ChildComponentProps = {
    pickEmoji: (emojiData: EmojiClickData, event: MouseEvent) => void
}

export default function EmojiPickerMenu(props: ChildComponentProps) {
    const {pickEmoji} = props;
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <IconButton
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
            >
                <InsertEmoticonIcon/>
            </IconButton>
            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <EmojiPicker onEmojiClick={pickEmoji}/>
            </Menu>
        </div>
    );
}
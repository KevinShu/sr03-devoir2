/* eslint-disable react/jsx-props-no-spreading */
import * as React from 'react'
import {Box} from '@mui/system'

export declare type CustomPageProps = React.PropsWithChildren<{}>

const CustomPage: React.FC<CustomPageProps> = ({
                                                   children
                                               }) => {

    return (
        <Box mt="7vh">
            {children}
        </Box>
    )
}

export default CustomPage

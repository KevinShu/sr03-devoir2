import {IconButton, Menu} from "@mui/material";
import MicIcon from "@mui/icons-material/Mic";
import {ReactMediaRecorder} from "react-media-recorder-2";
import * as React from "react";
import {useState} from "react";
import {Box} from "@mui/system";
import Fab from "@mui/material/Fab";
import ModeStandbyIcon from '@mui/icons-material/ModeStandby';
import GraphicEqIcon from '@mui/icons-material/GraphicEq';
import Button from "@mui/material/Button";


interface VoiceMessageMenuProps {
    inputMessage: any;
    setInputMessage: React.Dispatch<React.SetStateAction<string>>;
    handleMessageSubmit: (message: string) => void
}

function blobURLToDataURL(blobURL: string) {
    return new Promise((resolve, reject) => {
        fetch(blobURL)
            .then(response => response.blob())
            .then(blob => {
                const reader = new FileReader();
                reader.onloadend = () => {
                    resolve(reader.result);
                };
                reader.onerror = reject;
                reader.readAsDataURL(blob);
            })
            .catch(reject);
    });
}


const VoiceMessageMenu: React.FC<VoiceMessageMenuProps> = ({handleMessageSubmit}) => {

    const [recordingAnchorOpen, setRecordingAnchorOpen] = useState(null);
    const [isRecording, setIsRecording] = useState(false);
    const handleRecordingAnchor = (event: any) => {
        setRecordingAnchorOpen(event.currentTarget)
    }
    const handleRecordingAnchorClose = () => {
        setRecordingAnchorOpen(null)
    }
    const handleVoiceMessageSend = (blobUrl: string) => {
        console.log(blobUrl)
        if (blobUrl) {
            blobURLToDataURL(blobUrl)
                .then((dataURL: any) => {
                    console.log(dataURL);
                    handleMessageSubmit(dataURL);
                    setRecordingAnchorOpen(null);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        }
    }
    return (
        <Box>
            <IconButton onClick={handleRecordingAnchor}><MicIcon/></IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={recordingAnchorOpen}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                keepMounted
                transformOrigin={{vertical: 'top', horizontal: 'right'}}
                open={Boolean(recordingAnchorOpen)}
                onClose={handleRecordingAnchorClose}
            >
                <ReactMediaRecorder
                    audio
                    mediaRecorderOptions={{mimeType: 'audio/webm'}}
                    render={({status, startRecording, stopRecording, mediaBlobUrl, clearBlobUrl}) => (
                        <Box margin="2vh">
                            {mediaBlobUrl &&
                                <Box display="flex" justifyContent="space-between">
                                    <Box>
                                        <audio src={mediaBlobUrl} controls/>
                                    </Box>
                                    <Box display="flex">
                                        <Button color="error" onClick={() => {
                                            clearBlobUrl()
                                        }}>Delete</Button>
                                        <Button onClick={() => {
                                            handleVoiceMessageSend(mediaBlobUrl)
                                        }}>Send</Button>
                                    </Box>
                                </Box>
                            }
                            <Box display="flex" justifyContent="center">
                                {mediaBlobUrl === undefined && (status === "idle" || status === "stopped") &&
                                    <Fab variant="extended" size="small" color="primary" aria-label="add"
                                         onClick={() => {
                                             if (isRecording) {
                                                 setIsRecording(false)
                                                 stopRecording()
                                             } else {
                                                 setIsRecording(true)
                                                 startRecording()
                                             }
                                         }}>
                                        <ModeStandbyIcon sx={{mr: 1}}/>
                                        Record
                                    </Fab>}
                                {status === "recording" &&
                                    <Fab variant="extended" size="small" color="error" aria-label="add" onClick={() => {
                                        if (isRecording) {
                                            setIsRecording(false)
                                            stopRecording()
                                        } else {
                                            setIsRecording(true)
                                            startRecording()
                                        }
                                    }}>
                                        <GraphicEqIcon sx={{mr: 1}}/>
                                        Recording
                                    </Fab>}
                            </Box>
                        </Box>
                    )}
                />
            </Menu>
        </Box>
    )
}

export default VoiceMessageMenu;
import React, {useEffect, useState} from "react";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ZoomOutIcon from '@mui/icons-material/ZoomOut';
import ZoomInIcon from '@mui/icons-material/ZoomIn';
import {Box} from "@mui/system";
import {IconButton} from "@mui/material";

interface ImagePreviewDialogProps {
    open: boolean;
    setOpen: React.Dispatch<React.SetStateAction<boolean>>;
    images: any[];
    index: number;
    setIndex: React.Dispatch<React.SetStateAction<number>>;
}

const ImagePreviewDialog: React.FC<ImagePreviewDialogProps> = ({open, setOpen, images, index, setIndex}) => {

    const [nbImage, setNbImage] = useState(0);
    const [imageWidth, setImageWidth] = useState(400);

    useEffect(() => {
        setNbImage(images.length)
    }, [images])

    const handleClose = () => {
        setOpen(false);
    };

    const handlePreviousImage = () => {
        if (index > 0) {
            setIndex(index - 1)
        }
    }

    const handleNextImage = () => {
        if (index < nbImage - 1) {
            setIndex(index + 1)
        }
    }

    const handleZoomInImage = () => {
        if (!(imageWidth >= 1500))
            setImageWidth(imageWidth + 100);
    }

    const handleZoomOutImage = () => {
        if (!(imageWidth <= 200))
            setImageWidth(imageWidth - 100);
    }

    return (
        <div>
            <Dialog
                fullWidth
                maxWidth="lg"
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"View image"}
                </DialogTitle>
                <DialogContent>
                    <Box display="flex" justifyContent="center">
                        <Box width="10%">
                            <IconButton aria-label="previous-image" size="large" onClick={handlePreviousImage}>
                                <ArrowLeftIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                        <Box display="flex" width="100%" justifyContent="center">
                            <IconButton aria-label="zoom-out-image" size="large" onClick={handleZoomOutImage}>
                                <ZoomOutIcon fontSize="inherit"/>
                            </IconButton>
                            <IconButton aria-label="zoom-in-image" size="large" onClick={handleZoomInImage}>
                                <ZoomInIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                        <Box width="10%">
                            <IconButton aria-label="next-image" size="large" onClick={handleNextImage}>
                                <ArrowRightIcon fontSize="inherit"/>
                            </IconButton>
                        </Box>
                    </Box>
                    <Box>
                        <DialogContentText id="alert-dialog-description" textAlign="center">
                            {images && images[index] && images[index].sender}
                        </DialogContentText>
                        <DialogContentText textAlign="center">
                            {images && images[index] && images[index].date}
                        </DialogContentText>
                        <Box display="flex" justifyContent="center">
                            {images && <img src={images && images[index] && images[index].content} alt="img-view"
                                            style={{width: imageWidth}}/>}
                        </Box>
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default ImagePreviewDialog;
import React, {useEffect} from 'react'
import AppBar from '@mui/material/AppBar'
import Badge from '@mui/material/Badge'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import HomeIcon from '@mui/icons-material/Home'
import MenuIcon from '@mui/icons-material/Menu'
import {IconButton, Drawer, MenuItem, Menu, CircularProgress, Typography, MenuList} from '@mui/material'
import {useLocation, useNavigate} from 'react-router-dom'
import NotificationsIcon from '@mui/icons-material/Notifications'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import AccountCircle from '@mui/icons-material/AccountCircle'
import {APP_ROUTES} from 'router/Routes'
import {styled} from '@mui/system'
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import Avatar from "@mui/material/Avatar"
import ListItemText from "@mui/material/ListItemText"
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import CustomBreadcrumbs from "../molecule/CustomBreadcrumbs";
import UTCLogo from "../../assets/img/utcLogo.svg";
import Logo from "../../assets/img/Logo.png";
import {CurrentDateTime} from "../molecule/CurrentTime";
import SettingsIcon from "@mui/icons-material/Settings";
import ListItemIcon from "@mui/material/ListItemIcon";
import PeopleIcon from "@mui/icons-material/People";
import ChatIcon from "@mui/icons-material/Chat";

const drawerWidth = 200
const username = localStorage.getItem("email")

const StyledDrawer = styled(Drawer)(() => ({
    flexShrink: 0,
    width: drawerWidth,
    '& .MuiDrawer-paper': {
        width: drawerWidth,
    },
}));

export default function TopNavbarDesktop() {

    const navigate = useNavigate()
    const location = useLocation()
    const [open, setOpen] = React.useState(false)
    const [invitation, setInvitation] = React.useState([])
    const [isLoadingInfo, setIsLoadingInfo] = React.useState(false)
    const toggleDrawer = (event: any) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return
        }
        setOpen(!open)
    }
    const [accountAnchorEl, setAccountAnchorEl] = React.useState(null)
    const [infoAnchorEl, setInfoAnchorEl] = React.useState(null)
    const [currentInvitation, setCurrentInvitation] = React.useState<any>(null)
    const [joinConfirmationDialogOpen, setJoinConfirmationDialogOpen] = React.useState(false)
    const handleAccountMenu = (event: any) => {
        setAccountAnchorEl(event.currentTarget)
    }

    const handleInfoMenu = (event: any) => {
        setInfoAnchorEl(event.currentTarget)
    }
    const handleAccountMenuClose = () => {
        setAccountAnchorEl(null)
    }

    const handleInfoMenuClose = () => {
        setInfoAnchorEl(null)
    }

    const handleLogout = () => {
        handleAccountMenuClose()
        localStorage.removeItem('email')
        localStorage.removeItem('token')
        navigate(APP_ROUTES.LOGIN)
    }

    useEffect(() => {
        handleInfoLoad().then();
    }, [navigate])

    const handleInfoLoad = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/invitation/` + localStorage.getItem('email'), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
        });
        if (response.ok) {
            const data = await response.json();
            console.log("invitation info got : ", data);
            setInvitation(data)
            setIsLoadingInfo(false);
        } else {
            console.error('Get invitation list failed');
            setIsLoadingInfo(false);
        }
    }

    const handleMoreClick = (inv: any) => {
        setCurrentInvitation(inv)
        setJoinConfirmationDialogOpen(true)
    }

    const handleJoinConfirmationDialogClose = () => {
        setJoinConfirmationDialogOpen(false);
    }
    const handleJoinConfirmationDialogAccept = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/invitation/accept`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify({
                invitationId: currentInvitation.id,
            }),
        });
        if (response.ok) {
            setJoinConfirmationDialogOpen(false)
            console.log("Invitation accepted")
            window.location.reload()
        } else {
            console.log("Invitation accepting error")
        }
    }
    const handleJoinConfirmationDialogDecline = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/invitation/decline`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify({
                invitationId: currentInvitation.id,
            }),
        });
        if (response.ok) {
            setJoinConfirmationDialogOpen(false)
            console.log("Invitation declined")
            window.location.reload()
        } else {
            console.log("Invitation declining error")
        }
    }

    const handleAccountClick = () => {
        if (localStorage.getItem('email') && localStorage.getItem('token'))
            navigate(APP_ROUTES.ACCOUNT);
    }

    return (
        <div>
            {(location.pathname !== APP_ROUTES.LOGIN) && (location.pathname !== APP_ROUTES.REGISTER) && (
                <Box sx={{flexGrow: 1}}>
                    <AppBar position="fixed">
                        <Box sx={{
                            display: 'row',
                            alignItems: 'center',
                            marginLeft: 1,
                            marginRight: 1,
                        }}>
                            <Box sx={{
                                height: '4vh',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                flex: '1 0 auto'
                            }}>
                                <Toolbar>
                                    <IconButton size="large" edge="start" color="inherit" sx={{mr: 2}}
                                                onClick={toggleDrawer}>
                                        <MenuIcon/>
                                    </IconButton>
                                    <IconButton onClick={() => navigate(APP_ROUTES.HOME)}>
                                        <HomeIcon sx={{color: '#fff'}}/>
                                    </IconButton>
                                </Toolbar>
                                {location.pathname !== APP_ROUTES.LOGIN && (
                                    <Box sx={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        justifyContent: 'flex-end',
                                        alignItems: 'center',
                                        width: '60%',
                                        mr: 2
                                    }}>
                                        {isLoadingInfo && <CircularProgress size="small"/>}
                                        {!isLoadingInfo && invitation.length !== 0 ?
                                            <IconButton
                                                aria-label="notification of current user"
                                                aria-controls="menu-appbar"
                                                aria-haspopup="true"
                                                onClick={handleInfoMenu}
                                                color="inherit"
                                            >
                                                <Badge badgeContent={invitation.length} color="secondary">
                                                    <NotificationsIcon/>
                                                </Badge>
                                            </IconButton> :
                                            <IconButton
                                                aria-label="notification of current user"
                                                aria-controls="menu-appbar"
                                                aria-haspopup="true"
                                                onClick={handleInfoMenu}
                                                color="inherit"
                                            >
                                                <NotificationsIcon/>
                                            </IconButton>
                                        }
                                        <Menu
                                            id="menu-appbar"
                                            anchorEl={infoAnchorEl}
                                            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                                            keepMounted
                                            transformOrigin={{vertical: 'top', horizontal: 'right'}}
                                            open={Boolean(infoAnchorEl)}
                                            onClose={handleInfoMenuClose}
                                        >
                                            <List>
                                                {invitation.length === 0 &&
                                                    <Typography padding={1}>
                                                        Vous n'avez pas de notification.
                                                    </Typography>}
                                                {invitation.length !== 0 && invitation.map((inv: any) => {
                                                    return (
                                                        <ListItem
                                                            secondaryAction={
                                                                <IconButton edge="end" aria-label="more"
                                                                            onClick={() => {
                                                                                handleMoreClick(inv)
                                                                            }}>
                                                                    <MoreHorizIcon/>
                                                                </IconButton>
                                                            }
                                                        >
                                                            <ListItemAvatar>
                                                                <Avatar>
                                                                    <NotificationsNoneIcon/>
                                                                </Avatar>
                                                            </ListItemAvatar>
                                                            <ListItemText
                                                                primary={inv.conversation.name}
                                                                secondary={"Invité par " + inv.hostUser.usernameLine}
                                                            />
                                                        </ListItem>
                                                    )
                                                })}
                                            </List>
                                        </Menu>
                                        <IconButton
                                            aria-label="account of current user"
                                            aria-controls="menu-appbar"
                                            aria-haspopup="true"
                                            onClick={handleAccountMenu}
                                            color="inherit"
                                        >
                                            <AccountCircle/>
                                        </IconButton>
                                        <Menu
                                            id="menu-appbar"
                                            anchorEl={accountAnchorEl}
                                            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                                            keepMounted
                                            transformOrigin={{vertical: 'top', horizontal: 'right'}}
                                            open={Boolean(accountAnchorEl)}
                                            onClose={handleAccountMenuClose}
                                        >
                                            <MenuItem onClick={handleAccountClick}>{"My Account"}</MenuItem>
                                            <MenuItem onClick={handleLogout}>{"Logout"}</MenuItem>
                                        </Menu>
                                        {username}
                                    </Box>
                                )}
                            </Box>
                            <Box display="flex">
                                <CustomBreadcrumbs/>
                            </Box>
                        </Box>
                    </AppBar>
                    {open && (
                        <StyledDrawer
                            variant="temporary"
                            anchor="left"
                            open={open}
                            onClose={toggleDrawer}
                        >
                            <Box
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    height: '100%',
                                    width: '100%',
                                    padding: '16px',
                                }}
                            >
                                <Box>
                                    <Box display="row" justifyContent="center">
                                        <Box display="row">
                                            <Box>
                                                <img src={UTCLogo} alt="utcLogo" width="150"/>
                                            </Box>
                                            <Box>
                                                <img src={Logo} alt="Logo" width="150"/>
                                            </Box>
                                        </Box>
                                        <CurrentDateTime/>
                                        <MenuList>
                                            <MenuItem onClick={() => {
                                                setOpen(false)
                                                navigate('/chat')
                                            }}>
                                                <ListItemIcon>
                                                    <ChatIcon fontSize="small"/>
                                                </ListItemIcon>
                                                <ListItemText>Chat</ListItemText>
                                                {/*<Typography variant="body2" color="text.secondary">*/}
                                                {/*    Chat*/}
                                                {/*</Typography>*/}
                                            </MenuItem>
                                            <MenuItem onClick={() => {
                                                setOpen(false)
                                                navigate('/contact')
                                            }}>
                                                <ListItemIcon>
                                                    <PeopleIcon fontSize="small"/>
                                                </ListItemIcon>
                                                <ListItemText>Contact</ListItemText>
                                                {/*<Typography variant="body2" color="text.secondary">*/}
                                                {/*    Contact*/}
                                                {/*</Typography>*/}
                                            </MenuItem>
                                            <MenuItem onClick={() => {
                                                setOpen(false)
                                                navigate('/settings')
                                            }}>
                                                <ListItemIcon>
                                                    <SettingsIcon fontSize="small"/>
                                                </ListItemIcon>
                                                <ListItemText>Settings</ListItemText>
                                                {/*<Typography variant="body2" color="text.secondary">*/}
                                                {/*    Settings*/}
                                                {/*</Typography>*/}
                                            </MenuItem>
                                        </MenuList>
                                    </Box>
                                </Box>
                            </Box>
                        </StyledDrawer>
                    )}
                </Box>
            )}
            {currentInvitation !== null &&
                <Dialog
                    open={joinConfirmationDialogOpen}
                    onClose={handleJoinConfirmationDialogClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Rejoindre cette conversation ?"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {currentInvitation.hostUser.usernameLine} vous a invité à rejoindre la conversation
                            "{currentInvitation.conversation.name}".
                        </DialogContentText>
                        <br/>
                        <DialogContentText id="alert-dialog-description">
                            Les participants dans cette conversation :
                        </DialogContentText>
                        <List>
                            {currentInvitation.conversation.participants.map((participant: any) =>
                                <ListItemText>
                                    {participant.fullName}
                                </ListItemText>)}
                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleJoinConfirmationDialogClose}>Annuler</Button>
                        <Button onClick={handleJoinConfirmationDialogDecline}>Non</Button>
                        <Button onClick={handleJoinConfirmationDialogAccept} autoFocus>
                            Oui
                        </Button>
                    </DialogActions>
                </Dialog>
            }
        </div>
    )
}
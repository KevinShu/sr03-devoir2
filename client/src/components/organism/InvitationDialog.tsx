import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {Box} from "@mui/system";
import {useState} from "react";
import {
    CircularProgress,
    FormControl,
    FormHelperText,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput
} from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import Checkbox from "@mui/material/Checkbox";
import ListItemText from "@mui/material/ListItemText";
import AlertDialog from "./AlertDialog";
import AddIcon from "@mui/icons-material/Add";

type ChildComponentProps = {
    currentConversationId: any;
};

export default function InvitationDialog(props: ChildComponentProps) {
    const {currentConversationId} = props;
    const [open, setOpen] = React.useState(false);
    const [userList, setUserList] = useState<string[]>([]);
    const [isLoadingUserList, setIsLoadingUserList] = useState(true);
    const [checked, setChecked] = useState(['']);
    const [invitationCompleteDialogOpen, setInvitationCompleteDialogOpen] = useState(false);
    const [invitationFailedDialogOpen, setInvitationFailedDialogOpen] = useState(false);
    const [invitationSuccessMail, setInvitationSuccessMail] = useState([]);
    const [invitationFailedMail, setInvitationFailedMail] = useState([]);
    const [customUserEmail, setCustomUserEmail] = useState('');
    const [customUserEmailError, setCustomUserEmailError] = useState(false);


    const handleToggle = (value: string) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const handleClickOpen = () => {
        setOpen(true);
        handleGetUserList().then();
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleInvitation = async () => {
        setOpen(false);
        const invite_response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/conversation/invite`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify({
                user_host_email: localStorage.getItem('email'),
                user_invited_email: checked.slice(1),
                conversationId: currentConversationId
            }),
        });

        if (invite_response.ok) {
            const data = await invite_response.json();
            console.log("Users found and invited: ", data.success);
            console.log("Users not found or failed to invite: ", data.failed);
            setInvitationSuccessMail(data.success)
            setInvitationFailedMail(data.failed)
            setInvitationCompleteDialogOpen(true)
        } else {
            console.error('Invitation failed');
            setInvitationCompleteDialogOpen(true)
        }
    }

    const handleGetUserList = async () => {
        setIsLoadingUserList(true);
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/users`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
        });

        if (response.ok) {
            const data = await response.json();
            console.log("user list got : ", data);
            setUserList(data)
            setIsLoadingUserList(false);
        } else {
            console.error('Get User list failed');
            setIsLoadingUserList(false);
        }
    };
    const handleCustomUserEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCustomUserEmail(event.target.value);
        setCustomUserEmailError(!validateEmail(event.target.value));
    };

    const validateEmail = (email: string) => {
        // Simple validation check for email pattern
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    const handleClickAdd = () => {
        if (!userList.includes(customUserEmail)) {
            userList.push(customUserEmail)
        }
        const currentIndex = checked.indexOf(customUserEmail);
        const newChecked = [...checked];
        if (currentIndex === -1) {
            newChecked.push(customUserEmail);
        } else {
            newChecked.splice(currentIndex, 1);
        }
        setChecked(newChecked);
        setCustomUserEmail('');
    }

    return (
        <Box sx={{padding: '10px'}}>
            <Button variant="outlined" onClick={handleClickOpen} sx={{width: '100%'}}>
                Invite
            </Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Invitation</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please select users that you would like to invite:
                    </DialogContentText>
                    <Box display="flex" width="100%" justifyContent="center">
                        {isLoadingUserList ? <CircularProgress/> :
                            <Box sx={{width: '100%'}}>
                                <List sx={{width: '100%', bgcolor: 'background.paper'}}>
                                    {userList.map((value) => {
                                        const labelId = `checkbox-list-label-${value}`;
                                        if (value === localStorage.getItem('email')) return null;
                                        return (
                                            <ListItem
                                                key={value}
                                                disablePadding
                                            >
                                                <ListItemButton role={undefined} onClick={handleToggle(value)} dense>
                                                    <ListItemIcon>
                                                        <Checkbox
                                                            edge="start"
                                                            checked={checked.indexOf(value) !== -1}
                                                            tabIndex={-1}
                                                            disableRipple
                                                            inputProps={{'aria-labelledby': labelId}}
                                                        />
                                                    </ListItemIcon>
                                                    <ListItemText id={labelId} primary={value}/>
                                                </ListItemButton>
                                            </ListItem>
                                        );
                                    })}
                                </List>
                                <FormControl fullWidth variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-user">Add other email</InputLabel>
                                    <OutlinedInput
                                        required
                                        id="outlined-adornment-user"
                                        name="Add other email"
                                        label="Add other email"
                                        value={customUserEmail}
                                        onChange={handleCustomUserEmailChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="confirm"
                                                    onClick={handleClickAdd}
                                                    edge="end"
                                                >
                                                    <AddIcon/>
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                    <FormHelperText id="new-email-error">
                                        {customUserEmailError ? "Please enter a valid email address." : ""}
                                    </FormHelperText>
                                </FormControl>
                            </Box>
                        }
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={() => {
                        handleInvitation().then();
                    }}>Invite</Button>
                </DialogActions>
            </Dialog>
            <AlertDialog
                dialogTitle="Inivitation success"
                dialogBody={
                    (invitationSuccessMail.length === 0 ? "" :
                        "Les utilisateurs suivants ont été invités: " +
                        invitationSuccessMail.map(mail => "\n" + mail)) +
                    (invitationFailedMail.length === 0 ? "" : "Les utilisateurs suivants n'étaient pas trouvés: "
                        + invitationFailedMail.map(mail => "\n" + mail))
                }
                open={invitationCompleteDialogOpen}
                setOpen={setInvitationCompleteDialogOpen}
            />
            <AlertDialog
                dialogTitle="Inivitation failed"
                dialogBody={
                    "Il y a un problem pour inviter les utilisateur. Veuillez réssayer plus tard."
                }
                open={invitationFailedDialogOpen}
                setOpen={setInvitationFailedDialogOpen}
            />
        </Box>

    );
}
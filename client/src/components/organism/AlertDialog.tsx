import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

type ChildComponentProps = {
    dialogTitle: string,
    dialogBody: string,
    confirmText?: string,
    refreshOnClose?: boolean,
    open: boolean,
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
};

export default function AlertDialog(props: ChildComponentProps) {
    const {dialogTitle, dialogBody, open, setOpen, confirmText = "OK", refreshOnClose = false} = props

    return (
        <div>
            <Dialog
                open={open}
                onClose={() => {
                    setOpen(false)
                    if (refreshOnClose) {
                        window.location.reload();
                    }
                }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {dialogTitle}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {dialogBody}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        setOpen(false)
                        if (refreshOnClose) {
                            window.location.reload();
                        }
                    }} autoFocus>
                        {confirmText}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
import React, {useState} from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Typography} from "@mui/material";
import ImageIcon from '@mui/icons-material/Image';

interface ImageUploadMenuProps {
    inputMessage: any;
    setInputMessage: React.Dispatch<React.SetStateAction<string>>;
    handleMessageSubmit: (message: string) => void
}

const ImageUploadMenu: React.FC<ImageUploadMenuProps> = ({inputMessage, setInputMessage, handleMessageSubmit}) => {
    // const [selectedImage, setSelectedImage] = useState<File | null>(null);
    const [previewUrl, setPreviewUrl] = useState<string | null>(null);
    const [open, setOpen] = useState(false);
    const [inputKey, setInputKey] = useState(0);
    const [errorMessage, setErrorMessage] = useState('');

    const handleImageSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
        console.log("selected file: ", event.target.files, event.target.files && event.target.files[0])
        const file = event.target.files && event.target.files[0];
        if (file) {
            // setSelectedImage(file);
            const reader = new FileReader();
            reader.onload = () => {
                const sizeInKb = getDataUrlSize(reader.result);
                if (sizeInKb <= 1024) {
                    setPreviewUrl(reader.result as string);
                    setInputMessage(reader.result as string);
                    console.log(reader.result as string);
                } else {
                    // setSelectedImage(null);
                    setPreviewUrl(null);
                    setErrorMessage('The selected image exceeds the maximum size limit of 1024KB.');
                }
            };
            reader.readAsDataURL(file);
            setOpen(true);
        }
    };

    const handleSendImage = () => {
        if (inputMessage && inputMessage.startsWith("data:image")) {
            handleMessageSubmit(inputMessage)
            setOpen(false);
            // Clear the selected image and preview
            // setSelectedImage(null);
            setPreviewUrl(null);
            // Reset the input key to allow re-selection of the same image
            setInputKey((prevKey) => prevKey + 1);
        } else {
            console.log("Something went wrong")
        }
    };

    const handleCloseDialog = () => {
        setInputMessage('');
        setOpen(false);
        setTimeout(() => {
            // setSelectedImage(null);
            setPreviewUrl(null);
            // Reset the input key to allow re-selection of the same image
            setInputKey((prevKey) => prevKey + 1);
            setErrorMessage('');
        }, 500);
    };

    return (
        <div>
            <input
                type="file"
                accept="image/*"
                onChange={handleImageSelect}
                style={{display: "none"}}
                id="image-upload-input"
                key={inputKey}
            />
            <label htmlFor="image-upload-input">
                <IconButton component="span">
                    <ImageIcon/>
                </IconButton>
            </label>
            <Dialog open={open} onClose={handleCloseDialog}>
                <DialogTitle>Image Preview</DialogTitle>
                <DialogContent>
                    {previewUrl && <img src={previewUrl} alt="Preview" style={{width: "200px"}}/>}
                    {errorMessage && <Typography color="error">{errorMessage}</Typography>}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog}>Cancel</Button>
                    {!errorMessage && <Button onClick={handleSendImage}>Send Image</Button>}
                </DialogActions>
            </Dialog>
        </div>
    );
};

function getDataUrlSize(dataUrl: any) {
    const blob = dataURLToBlob(dataUrl);
    const sizeInBytes = blob.size;
    return Math.round(sizeInBytes / 1024);
}

function dataURLToBlob(dataUrl: any) {
    const parts = dataUrl.split(',');
    const contentType = parts[0].match(/:(.*?);/)[1];
    const base64Data = atob(parts[1]);
    const arrayBuffer = new ArrayBuffer(base64Data.length);
    const uint8Array = new Uint8Array(arrayBuffer);

    for (let i = 0; i < base64Data.length; i++) {
        uint8Array[i] = base64Data.charCodeAt(i);
    }

    return new Blob([arrayBuffer], {type: contentType});
}

export default ImageUploadMenu;
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import Fab from '@mui/material/Fab';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import PersonIcon from '@mui/icons-material/Person';
import ForumIcon from '@mui/icons-material/Forum';
import ListItemText from '@mui/material/ListItemText';
import TextField from '@mui/material/TextField';
import SendIcon from '@mui/icons-material/Send';
import CustomPage from "../../components/organism/CustomPage";
import ListItemButton from "@mui/material/ListItemButton";
import {useEffect, useRef, useState} from "react";
import {useNavigate} from "react-router-dom";
import {APP_ROUTES} from "../../router/Routes";
import {Box, styled} from "@mui/system";
import {CircularProgress, Typography} from "@mui/material";
import {Client, over} from 'webstomp-client';
import SockJS from "sockjs-client";
import NewConversationFormDialog from "../../components/organism/NewConversationFormDialog";
import Button from "@mui/material/Button";
import EmojiPickerMenu from "../../components/organism/EmojiPickerMenu";
import {EmojiClickData} from "emoji-picker-react/src/types/exposedTypes";
import QuitConversationDialog from "../../components/organism/QuitConversationDialog";
import InvitationDialog from "../../components/organism/InvitationDialog";
import ImageUploadMenu from "../../components/organism/ImageUploadMenu";
import ImagePreviewDialog from "../../components/organism/ImagePreviewDialog";
import VoiceMessageMenu from "../../components/organism/VoiceMessageMenu";

type Message = {
    content: string;
    sender: string;
    senderMail: string;
    id: number;
    date: string;
    conversationIdNumber: string;
};


const Chat: React.FC = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [chat, setChat] = useState([]);
    const [filteredChat, setFilteredChat] = useState([]);
    const [inputMessage, setInputMessage] = useState('');
    const [messages, setMessages] = useState<Message[]>([]);
    const messageListStartRef = useRef<HTMLDivElement>(null);
    const messageListEndRef = useRef<HTMLDivElement>(null);
    const conversationListStartRef = useRef<HTMLDivElement>(null);
    const conversationListEndRef = useRef<HTMLDivElement>(null);
    const navigate = useNavigate();
    const [client, setClient] = useState<Client | null>(null);
    const [currentConversationId, setCurrentConversationId] = useState<number | null>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    const [cursorPositon, setCursorPosition] = useState<number>(0);
    const [imageDialogOpen, setImageDialogOpen] = useState(false);
    const [imageIndex, setImageIndex] = useState(0);
    // const speech = require('@google-cloud/speech');

    // const transcribeSpeechFromAudioUrl = async (audioUrl: string) => {
    //     const client = new speech.SpeechClient();
    //
    //     const config = {
    //         encoding: 'LINEAR16',
    //         sampleRateHertz: 16000,
    //         languageCode: 'en-US',
    //     };
    //
    //     const audio = {
    //         uri: audioUrl,
    //     };
    //
    //     const request = {
    //         config: config,
    //         audio: audio,
    //     };
    //
    //     const [response] = await client.recognize(request);
    //     const transcription = response.results
    //         .map((result: any) => result.alternatives[0].transcript)
    //         .join('\n');
    //
    //     return transcription;
    // };

    const AnimatedButton = styled(Button)({
        transition: 'transform 0.2s',
        '&:hover': {
            transform: 'scale(1.05)',
        },
    });
    const handleNewMessageIntegration = (newMessage: Message) => {
        setChat(prevChat => {
            const chatCopy: any = [...prevChat]; // Copy the previous state
            const conversationIndex = chatCopy.findIndex((c: any) => c.id === newMessage.conversationIdNumber);

            if (conversationIndex !== -1) {
                // Copy the conversation and add the new message
                const conversationCopy = {...chatCopy[conversationIndex]};
                const {conversationIdNumber, ...updatedMessage} = newMessage;
                conversationCopy.messages = [...conversationCopy.messages, updatedMessage];

                // Replace the conversation in the chat
                chatCopy[conversationIndex] = conversationCopy;
            }

            return chatCopy;
        });
    }

    const handleImageClick = (messageId: number) => {
        let imagesInConersation = messages.filter((message: any) => {
            return (message.content.startsWith("data:image"));
        });
        let index = imagesInConersation.map((image: any) => (image.id)).indexOf(messageId)
        setImageIndex(index);
        setImageDialogOpen(true);
    }

    useEffect(() => {
        setFilteredChat(chat)
    }, [chat])

    useEffect(() => {
        scrollMessageListToBottom();
    }, [messages]);

    useEffect(() => {
        setIsLoading(true);

        // Fetch chat data and establish WebSocket connection
        const fetchDataAndConnectWebSocket = () => {
            fetch("http://localhost:8080/api/chat/" + localStorage.getItem('email'), {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }),
            })
                .then((res) => {
                    if (res.status === 401 || res.statusText === "TOKEN_EXPIRED") {
                        localStorage.removeItem('email');
                        localStorage.removeItem('token');
                        navigate(APP_ROUTES.LOGIN);
                        return [];
                    }
                    return res.json();
                })
                .then((result) => {
                    setChat(result);
                    establishWebSocketConnection();
                    setIsLoading(false);
                })
                .catch((error) => {
                    console.log(error);
                    setIsLoading(false);
                });
        };

        // Establish WebSocket connection
        const establishWebSocketConnection = () => {
            const socket = new SockJS("http://localhost:8080/ws");
            const websocketClient: Client = over(socket);

            websocketClient.connect({'STOMP': '1.2'}, () => {
                websocketClient.subscribe("/user/" + localStorage.getItem('email') + "/queue/messages", (message: any) => {
                    if (message.body) {
                        const newMessage = JSON.parse(message.body);
                        console.log("New message received: ", newMessage);
                        handleNewMessageIntegration(newMessage);
                    }
                });
                setClient(websocketClient);
            }, (error: any) => {
                console.error("STOMP error", error);
            });
        };

        // Clean up previous WebSocket connection on component unmount or before re-establishing
        const cleanupWebSocketConnection = () => {
            if (client) {
                client.disconnect(() => {
                    console.log("WebSocket connection closed");
                });
            }
        };

        // Clean up previous WebSocket connection and fetch new data on component mount or when 'navigate' changes
        cleanupWebSocketConnection();
        fetchDataAndConnectWebSocket();

        return () => {
            // Clean up WebSocket connection on component unmount
            cleanupWebSocketConnection();
        };
    }, [navigate]);

    useEffect(() => {
        setMessages([]); // Clear out the current messages state
        // filter the corresponding messages using the conversation's ID
        let conversation: any = chat.filter((conversation: any) => (conversation.id === currentConversationId));
        if (conversation.length > 0) {
            let messages = conversation[0].messages;
            setMessages(messages);
        } else {
            setMessages([]);
        }
    }, [chat, currentConversationId])

    const sentMessageStyle: React.CSSProperties = {
        backgroundColor: '#4fc3f7',
        color: 'white',
        padding: '10px',
        borderRadius: '10px',
        display: 'inline-block',
        maxWidth: '80%',
        wordWrap: 'break-word',
    };

    const receivedMessageStyle: React.CSSProperties = {
        backgroundColor: '#e0e0e0',
        color: 'black',
        padding: '10px',
        borderRadius: '10px',
        display: 'inline-block',
        maxWidth: '80%',
        wordWrap: 'break-word',
    };


    const handleConversationClick = (id: any) => {
        setCurrentConversationId(id);
        setMessages([]); // Clear out the current messages state
        // filter the corresponding messages using the conversation's ID
        let conversation: any = chat.filter((conversation: any) => (conversation.id === id));
        if (conversation.length > 0) {
            let messages = conversation[0].messages;
            setMessages(messages);
        } else {
            setMessages([]);
        }
    }

    const handleMessageSubmit = (message: string) => {
        if (client && currentConversationId && (inputMessage !== '' || message.startsWith("data:audio"))) {
            // Prepare the message object to be sent to the server
            const messageDto = {
                sender: localStorage.getItem('email'),
                senderMail: localStorage.getItem('email'),
                content: message,
                conversationId: currentConversationId,
                date: new Date().toISOString()
            };
            client.send("/app/chat", JSON.stringify(messageDto));
        }
        setInputMessage('');
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            handleMessageSubmit(inputMessage);
        }
    };

    const scrollMessageListToTop = () => {
        if (messageListStartRef.current) {
            messageListStartRef.current.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    };

    const scrollMessageListToBottom = () => {
        if (messageListEndRef.current) {
            messageListEndRef.current.scrollIntoView({behavior: 'smooth', block: 'end'});
        }
    };

    const scrollConversationListToTop = () => {
        if (conversationListStartRef.current) {
            conversationListStartRef.current.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    };

    const scrollConversationListToBottom = () => {
        if (conversationListEndRef.current) {
            conversationListEndRef.current.scrollIntoView({behavior: 'smooth', block: 'end'});
        }
    };

    const pickEmoji = (emojiData: EmojiClickData, event: MouseEvent) => {
        event.stopPropagation();
        const ref: any = inputRef.current;
        const startPos = ref.selectionStart || 0;
        const endPos = ref.selectionEnd || 0;
        const message = inputMessage.substring(0, startPos) + emojiData.emoji + inputMessage.substring(endPos);
        setInputMessage(message);
        const newCursorPosition = startPos + emojiData.emoji.length;
        setCursorPosition(newCursorPosition);
        ref.focus();
        setTimeout(() => {
            ref.setSelectionRange(newCursorPosition, newCursorPosition);
        }, 0);
    }

    const handleSearchFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const conversations = chat.filter((conversation: any) => {
            const filteredMessages = conversation.messages.filter((message: any) => {
                if (message.content.startsWith("data:image") || message.content.startsWith("data:audio")) {
                    return false; // Exclure les messages de type image
                }
                return message.content.includes(event.target.value);
            });

            // Vérifier également si la valeur de recherche est présente dans les autres champs de la conversation
            const conversationValues = Object.values(conversation);
            const foundInConversation = conversationValues.some((value) => {
                if (typeof value === "string") {
                    return value.includes(event.target.value);
                }
                return false;
            });

            return filteredMessages.length > 0 || foundInConversation;
        });
        // Les conversations qui correspondent à la valeur de recherche
        setFilteredChat(conversations);
    }

    if (isLoading) {
        return (
            <CustomPage>
                <Box width="100%" justifyContent="center" display="flex">
                    <CircularProgress/>
                </Box>
            </CustomPage>)
    }
    return (
        <CustomPage>
            <Grid container sx={{width: '100%', height: '80vh'}}>
                <Grid item xs={2.5}
                      sx={{borderRight: '1px solid #e0e0e0', height: '93vh', overflowX: 'hidden', overflowY: 'auto'}}>
                    <div ref={conversationListStartRef}/>
                    <Grid item xs={12} sx={{padding: '10px'}}>
                        <TextField id="outlined-basic-email" label="Search" variant="outlined"
                                   onChange={handleSearchFieldChange} fullWidth/>
                    </Grid>
                    <Divider/>
                    <NewConversationFormDialog chat={chat}
                                               setChat={setChat}
                                               currentConversationId={currentConversationId}
                                               setCurrentConversationId={setCurrentConversationId}
                                               scrollConversationListToBottom={scrollConversationListToBottom}/>
                    <List>
                        {filteredChat.map((conversation: any) => {
                            let lastMessage = {content: "No messages yet", id: -1, date: ""}; // default message
                            if (conversation != null && conversation.messages != null && conversation.messages.length > 0) {
                                lastMessage = conversation.messages.reduce((prev: any, current: any) => {
                                    return (prev.id > current.id) ? prev : current;
                                });
                            }
                            return (
                                currentConversationId === conversation.id ?
                                    <ListItemButton sx={{backgroundColor: 'lightgrey'}}
                                                    key={conversation.id}
                                                    onClick={() => handleConversationClick(conversation.id)}>
                                        <ListItemIcon>
                                            {conversation.participants.length === 2 ? <Avatar alt="conversation">
                                                <PersonIcon/>
                                            </Avatar> : <Avatar alt="conversation">
                                                <ForumIcon/>
                                            </Avatar>}
                                        </ListItemIcon>
                                        {conversation.participants.length === 2 ?
                                            <Box>
                                                <ListItemText
                                                    primary={conversation ? conversation.participants.map((p: any) => {
                                                        if (p.mail !== localStorage.getItem('email')) return p.fullName;
                                                        return null;
                                                    }) : "UNNAMED"}
                                                    secondary={lastMessage.content}/>
                                                <ListItemText secondary={lastMessage.date.replace("T", " ")}/>
                                            </Box> : <Box>
                                                <ListItemText primary={conversation ? conversation.name : "UNNAMED"}
                                                              secondary={lastMessage.content}/>
                                                <ListItemText secondary={lastMessage.date.replace("T", " ")}/>
                                            </Box>}

                                    </ListItemButton> :
                                    <ListItemButton key={conversation.id}
                                                    onClick={() => handleConversationClick(conversation.id)}>
                                        <ListItemIcon>
                                            {conversation.participants.length === 2 ? <Avatar alt="conversation">
                                                <PersonIcon/>
                                            </Avatar> : <Avatar alt="conversation">
                                                <ForumIcon/>
                                            </Avatar>}
                                        </ListItemIcon>
                                        {conversation.participants.length === 2 ?
                                            <Box>
                                                <ListItemText
                                                    primary={conversation ? conversation.participants.map((p: any) => {
                                                        if (p.mail !== localStorage.getItem('email')) return p.fullName;
                                                        return null;
                                                    }) : "UNNAMED"}
                                                    secondary={lastMessage.content}/>
                                                <ListItemText secondary={lastMessage.date.replace("T", " ")}/>
                                            </Box> : <Box>
                                                <ListItemText primary={conversation ? conversation.name : "UNNAMED"}
                                                              secondary={lastMessage.content}/>
                                                <ListItemText secondary={lastMessage.date.replace("T", " ")}/>
                                            </Box>}
                                    </ListItemButton>
                            )
                        })}
                        <Divider/>
                        <Box display="flex" justifyContent="center">
                            <Button variant="text" onClick={scrollConversationListToTop}>Back to top</Button>
                        </Box>
                        <div ref={conversationListEndRef}/>
                    </List>
                </Grid>
                <Grid item xs={7} sx={{borderRight: '1px solid #e0e0e0', height: '93vh', overflowY: 'hidden'}}>
                    <Box display="flex" position="sticky" justifyContent="center" onClick={scrollMessageListToTop}>
                        <Typography fontSize={20} paddingTop={0.5} paddingLeft={2}>{chat.map((conv: any) => {
                            if (conv.id === currentConversationId) {
                                if (conv.participants.length === 2) {
                                    return conv.participants.map((p: any) => {
                                        if (p.mail !== localStorage.getItem('email')) return p.fullName + " (" + conv.name + ")";
                                        return null;
                                    })
                                }
                                return conv.name;
                            }
                        })
                        }</Typography>
                    </Box>
                    <List sx={{height: '70vh', overflowY: 'auto'}}>
                        <div ref={messageListStartRef}/>
                        {messages.map((msg: any) => {
                            const isCurrentUser = msg.senderMail === localStorage.getItem('email');
                            return (
                                <ListItem key={msg.id}>
                                    <Grid container direction="column"
                                          alignItems={isCurrentUser ? "flex-end" : "flex-start"}>
                                        <Grid item xs={12}>
                                            <div style={isCurrentUser ? sentMessageStyle : receivedMessageStyle}>
                                                {msg.content.startsWith("data:image") &&
                                                    <Box display="flex" justifyContent="center" onClick={() => {
                                                        handleImageClick(msg.id);
                                                    }}><img src={msg.content}
                                                            alt="img"
                                                            style={{width: "150px"}}/></Box>}
                                                {msg.content.startsWith("data:audio") &&
                                                    <Box display="flex" justifyContent="center">
                                                        <audio src={msg.content} controls/>
                                                        {/*<IconButton onClick={()=>{transcribeSpeechFromAudioUrl(msg.content).then(r => console.log(r))}}>*/}
                                                        {/*    <VoiceChatIcon/>*/}
                                                        {/*</IconButton>*/}
                                                    </Box>}
                                                {!msg.content.startsWith("data:image") && !msg.content.startsWith("data:audio") &&
                                                    <ListItemText primary={msg.content}></ListItemText>}
                                            </div>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Grid container justifyContent={isCurrentUser ? "flex-end" : "flex-start"}>
                                                <ListItemText secondary={msg.sender}></ListItemText>
                                                {msg.date != null && <ListItemText
                                                    secondary={msg.date.replace("T", " ")}></ListItemText>}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                            );
                        })}
                        {/*<Box display="flex" justifyContent="center">*/}
                        {/*<Button variant="text" size="small" onClick={scrollMessageListToTop}>Back to top</Button>*/}
                        {/*</Box>*/}
                        <div ref={messageListEndRef}/>
                    </List>
                    <Divider/>
                    <Grid container sx={{padding: '20px'}}>
                        <Grid item xs={11}>
                            <Box display="flex" alignItems="center" justifyContent="flex-start">
                                <VoiceMessageMenu inputMessage={inputMessage} handleMessageSubmit={handleMessageSubmit}
                                                  setInputMessage={setInputMessage}/>
                                <TextField fullWidth id="message-input" label="Message" value={inputMessage}
                                           onChange={(e) => setInputMessage(e.target.value)} onKeyDown={handleKeyPress}
                                           inputRef={inputRef} InputLabelProps={{shrink: true}}/>
                                <EmojiPickerMenu pickEmoji={pickEmoji}/>
                                <ImageUploadMenu inputMessage={inputMessage} handleMessageSubmit={handleMessageSubmit}
                                                 setInputMessage={setInputMessage}/>
                            </Box>
                        </Grid>
                        <Grid container justifyContent="flex-end">
                            <Grid item xs={1}>
                                <Fab color="primary" aria-label="add" onClick={() => handleMessageSubmit(inputMessage)}><SendIcon/></Fab>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={2.5}>
                    <Box>
                        <Typography fontSize={15} paddingTop={0.5} paddingLeft={2}>
                            Participants
                        </Typography>
                    </Box>
                    <Box display="row" justifyContent="center">
                        <Grid container padding={3} spacing={2}>
                            {chat.map((conv: any) => {
                                if (conv.id === currentConversationId) {
                                    if (conv.participants !== null && conv.participants.length !== 0) {
                                        return (
                                            <Box>
                                                {conv.participants.map((user: any) => (
                                                    <AnimatedButton
                                                        key={user.id}
                                                        sx={{
                                                            display: 'block',
                                                            borderRadius: 'borderRadius',
                                                            textAlign: 'center',
                                                            width: '50%'
                                                        }}
                                                    >
                                                        <Box display="flex"
                                                             flexDirection="column"
                                                             alignItems="center"
                                                             justifyContent="center">
                                                            <Avatar alt="user">
                                                                <PersonIcon/>
                                                            </Avatar>
                                                            <Typography fontSize={10}>{user.fullName}</Typography>
                                                            <Typography fontSize={8}>{user.mail}</Typography>
                                                        </Box>
                                                    </AnimatedButton>
                                                ))}
                                                <InvitationDialog
                                                    currentConversationId={currentConversationId}
                                                />
                                                <QuitConversationDialog chat={chat}
                                                                        setChat={setChat}
                                                                        currentConversationId={currentConversationId}
                                                                        setCurrentConversationId={setCurrentConversationId}
                                                                        scrollConversationListToBottom={scrollConversationListToBottom}/>
                                                <ImagePreviewDialog open={imageDialogOpen}
                                                                    setOpen={setImageDialogOpen}
                                                                    images={
                                                                        messages.filter((message: any) => {
                                                                            return message.content.startsWith("data:image")
                                                                        })
                                                                    }
                                                                    index={imageIndex}
                                                                    setIndex={setImageIndex}
                                                />
                                            </Box>
                                        )
                                    } else {
                                        return (
                                            <Typography>No participants</Typography>
                                        )
                                    }
                                }
                                }
                            )}
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </CustomPage>
    );
}

export default Chat;
import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Checkbox from '@mui/material/Checkbox';
import Avatar from '@mui/material/Avatar';
import {Box} from "@mui/system";
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import PersonIcon from '@mui/icons-material/Person';
import {APP_ROUTES} from "router/Routes";
import CustomPage from "components/organism/CustomPage";
import {CircularProgress} from "@mui/material";

const Contact: React.FC = () => {

    const [isLoading, setIsLoading] = useState(false);
    const [users, setUsers] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        setIsLoading(true);
        fetch("http://localhost:8080/api/users", {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }),
        })
            .then((res) => {
                if (res.status === 401 || res.statusText === "TOKEN_EXPIRED") {
                    localStorage.removeItem('email')
                    localStorage.removeItem('token')
                    // If the token has expired, redirect the user to the login page
                    navigate(APP_ROUTES.LOGIN);
                    return [];
                }
                return res.json();
            })
            .then(
                (result) => {
                    setUsers(result);
                    setIsLoading(false);
                },
                (error) => {
                    console.log(error);
                    setIsLoading(false);
                    return
                }
            )
    }, [navigate]);
    if (isLoading) {
        return (
            <CustomPage>
                <Box width="100%" justifyContent="center" display="flex">
                    <CircularProgress/>
                </Box>
            </CustomPage>)
    }
    return (
        <CustomPage>
            <Box width="100%" justifyContent="center" display="flex">
                <List dense sx={{width: '100%', maxWidth: 800, bgcolor: 'background.paper'}}>
                    {users.map((value) => {
                        const labelId = `checkbox-list-secondary-label-${value}`;
                        return (
                            <ListItem
                                key={value}
                                disablePadding
                            >
                                <ListItemButton>
                                    <ListItemAvatar>
                                        <Avatar
                                            alt={`Avatar n°${value + 1}`}
                                        >
                                            <PersonIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText id={labelId} primary={`${value}`}/>
                                </ListItemButton>
                            </ListItem>
                        );
                    })}
                </List>
            </Box>
        </CustomPage>
    );
}

export default Contact;
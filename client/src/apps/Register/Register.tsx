import * as React from 'react';
import {Box} from "@mui/system";
import Grid from "@mui/material/Grid";
import UTCLogo from "../../assets/img/utcLogo.svg";
import Logo from "../../assets/img/Logo.png";
import TextField from "@mui/material/TextField";
import Container from '@mui/material/Container';
import {
    CircularProgress,
    FormControl,
    FormHelperText,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Typography,
} from "@mui/material";
import {APP_ROUTES} from "../../router/Routes";
import Button from "@mui/material/Button";
import AlertDialog from "../../components/organism/AlertDialog";
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import CheckIcon from '@mui/icons-material/Check';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import DangerousIcon from '@mui/icons-material/Dangerous';
import EmailIcon from '@mui/icons-material/Email';
import PasswordIcon from '@mui/icons-material/Password';
import LoginIcon from '@mui/icons-material/Login';
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";

const Register: React.FC = () => {

    const [email, setEmail] = useState('');
    const navigate = useNavigate();
    const [error, setError] = useState('');
    const [emailError, setEmailError] = useState(false);
    const [isEmailChecking, setIsEmailChecking] = useState(false);
    const [emailAvailable, setEmailAvailable] = useState<boolean | null>(null);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [registerErrorDialogOpen, setRegisterErrorDialogOpen] = useState(false);
    const [registerSuccessDialogOpen, setRegisterSuccessDialogOpen] = useState(false);

    const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
        setEmailError(!validateEmail(event.target.value));
    };

    const handleFirstNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFirstName(event.target.value);
    };

    const handleLastNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setLastName(event.target.value);
    };

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        setIsLoading(true);
        event.preventDefault();
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/registration/new`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                mail: email,
                firstName: firstName,
                lastName: lastName,
            }),
        }).catch((error: any) => {
            setError(error);
            setIsLoading(false);
            setRegisterErrorDialogOpen(true);
        });

        if (response && response.ok) {
            setIsLoading(false);
            setRegisterSuccessDialogOpen(true);
        } else if (!response) {
            setError("There are no response from server, please try again later.");
            setIsLoading(false);
            setRegisterErrorDialogOpen(true);
            console.error('Failed to register');
        } else {
            setError("We are not able process your registration request, please try again later.");
            setIsLoading(false);
            setRegisterErrorDialogOpen(true);
            console.error('Failed to register');
        }
    };

    const validateEmail = (email: string) => {
        // Simple validation check for email pattern
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };


    useEffect(() => {
        // check email availability
        const checkEmailAvailability = async () => {
            setIsEmailChecking(true)
            try {
                const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/registration/checkmail`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        mail: email
                    }),
                });
                if (response.ok) {
                    setIsEmailChecking(false);
                    setEmailAvailable(true);
                } else if (response.status === 406) {
                    setIsEmailChecking(false);
                    setEmailAvailable(false);
                } else {
                    setIsEmailChecking(false);
                    setEmailAvailable(null);
                }
            } catch (error) {
                console.error('Error checking email availability:', error);
            }
        };

        if (email && validateEmail(email)) {
            checkEmailAvailability().then();
        }
    }, [email]);

    return (
        <Box>
            <Typography
                style={{
                    color: '#f5f5f5',
                    left: 0,
                    fontSize: '150px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                WELCOME,
            </Typography>
            <Typography
                style={{
                    color: '#f5f5f5',
                    left: 0,
                    bottom: 0,
                    fontSize: '90px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                GLAD TO HAVE YOU AMONG US
            </Typography>
            <Container component="main" maxWidth="xs">
                <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" height="90vh">
                    <Box width="100%" justifyContent="center" display="flex">
                        <form onSubmit={handleSubmit}>
                            <Grid container spacing={2}>
                                <Box display="flex" justifyContent="center">
                                    <img src={UTCLogo} alt="utcLogo" width="200"/>
                                    <img src={Logo} alt="Logo" width="200"/>
                                </Box>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined">
                                        <InputLabel error={emailError || emailAvailable === false}
                                                    htmlFor="outlined-adornment-password">Email</InputLabel>
                                        <OutlinedInput
                                            required
                                            id="outlined-adornment-email"
                                            name="email"
                                            label="Email"
                                            type="email"
                                            onChange={handleEmailChange}
                                            error={emailError || emailAvailable === false}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    {isEmailChecking && <CircularProgress/>}
                                                    {!isEmailChecking && !emailError && emailAvailable !== null && (emailAvailable ?
                                                        <CheckIcon color="success"/> : <DangerousIcon color="error"/>)}
                                                </InputAdornment>
                                            }
                                        />
                                        <FormHelperText error={emailError || emailAvailable === false}
                                                        id="accountId-error">
                                            {emailError ? "Please enter a valid email address." : ""}
                                            {!emailError && emailAvailable !== null && emailAvailable ? "This email is available." : ""}
                                            {!emailError && emailAvailable !== null && !emailAvailable ? "This email is not available." : ""}
                                        </FormHelperText>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="firstName"
                                        label="First Name"
                                        name="firstName"
                                        autoComplete="firstName"
                                        autoFocus
                                        value={firstName}
                                        onChange={handleFirstNameChange}
                                        helperText={firstName == null || firstName === '' ? "Please enter a valid name." : ""}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="lastName"
                                        label="Last Name"
                                        name="lastName"
                                        autoComplete="lastName"
                                        autoFocus
                                        value={lastName}
                                        onChange={handleLastNameChange}
                                        helperText={lastName == null || lastName === '' ? "Please enter a valid name." : ""}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Button type="submit" fullWidth variant="contained" color="primary"
                                            disabled={emailError || lastName == null || lastName === '' || firstName == null || firstName === '' || email == null || email === ''}>
                                        {isLoading ? (
                                            <CircularProgress size={24} color="inherit"/>
                                        ) : (
                                            'Register'
                                        )}
                                    </Button>
                                    <AlertDialog dialogTitle="Register failed" dialogBody={error}
                                                 open={registerErrorDialogOpen}
                                                 setOpen={setRegisterErrorDialogOpen}/>
                                    <Dialog
                                        open={registerSuccessDialogOpen}
                                        onClose={() => {
                                            setRegisterSuccessDialogOpen(false)
                                        }}
                                        aria-labelledby="alert-dialog-title"
                                        aria-describedby="alert-dialog-description"
                                    >
                                        <DialogTitle id="alert-dialog-title">
                                            Registration Success
                                        </DialogTitle>
                                        <DialogContent>
                                            <Box>
                                                <Box display="flex" justifyContent="center">
                                                    <CheckCircleOutlineIcon color="success" sx={{fontSize: "100px"}}/>
                                                </Box>
                                                <br/>
                                                <Box display="flex" justifyContent="center">
                                                    <DialogContentText id="alert-dialog-description">
                                                        Registration success.
                                                    </DialogContentText>
                                                </Box>
                                                <br/>
                                                <Box display="flex" alignItems="center">
                                                    <Box marginLeft="3vh">
                                                        <EmailIcon sx={{fontSize: "50px"}}/>
                                                    </Box>
                                                    <Box marginLeft="5vh">
                                                        <DialogContentText id="alert-dialog-email">
                                                            You will receive an email shortly where you can found your
                                                            credentials.
                                                        </DialogContentText>
                                                    </Box>
                                                </Box>
                                                <br/>

                                                <Box display="flex" alignItems="center">
                                                    <Box marginLeft="3vh">
                                                        <PasswordIcon sx={{fontSize: "50px"}}/>
                                                    </Box>
                                                    <Box marginLeft="5vh">
                                                        <DialogContentText id="alert-dialog-password">
                                                            For security concerns, please change your password
                                                            immediately after you login.
                                                        </DialogContentText>
                                                    </Box>
                                                </Box>
                                                <br/>
                                                <Box display="flex" alignItems="center">
                                                    <Box marginLeft="3vh">
                                                        <LoginIcon sx={{fontSize: "50px"}}/>
                                                    </Box>
                                                    <Box marginLeft="5vh">
                                                        <DialogContentText id="alert-dialog-login">
                                                            You will now be redirected to the login page.
                                                        </DialogContentText>
                                                    </Box>
                                                </Box>
                                            </Box>
                                        </DialogContent>
                                        <DialogActions>
                                            <Button onClick={() => {
                                                setRegisterSuccessDialogOpen(false)
                                                navigate(APP_ROUTES.LOGIN)
                                            }} autoFocus>
                                                OK
                                            </Button>
                                        </DialogActions>
                                    </Dialog>
                                </Grid>
                            </Grid>
                        </form>
                    </Box>
                </Box>
            </Container>
        </Box>
    );
}

export default Register;
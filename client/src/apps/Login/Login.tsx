import React, {useState} from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import UTCLogo from "../../assets/img/utcLogo.svg"
import Logo from "../../assets/img/Logo.png"
import {useNavigate} from 'react-router-dom';
import {APP_ROUTES} from 'router/Routes'
import {Box} from "@mui/system";
import Visibility from '@mui/icons-material/Visibility';
import {
    CircularProgress,
    FormControl,
    FormHelperText,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput, Paper, Typography
} from "@mui/material";
import {VisibilityOff} from "@mui/icons-material";
import AlertDialog from "../../components/organism/AlertDialog";
import Link from "@mui/material/Link";

const Login: React.FC = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const [error, setError] = useState('');
    const [emailError, setEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [showPassword, setShowPassword] = React.useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [loginErrorDialogOpen, setLoginErrorDialogOpen] = useState(false);
    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
        setEmailError(!validateEmail(event.target.value));
    };

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
        setPasswordError(event.target.value === '');
    };

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        setIsLoading(true);
        event.preventDefault();
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                mail: email,
                password: password,
            }),
        }).catch((error: any) => {
            setError(error);
            setIsLoading(false);
            setLoginErrorDialogOpen(true);
        });

        if (response && response.ok) {
            const data = await response.json();
            // Store the token in the local storage
            localStorage.setItem('email', email);
            localStorage.setItem('token', data.token);
            setIsLoading(false);
            // Redirect to homepage
            navigate(APP_ROUTES.HOME);
        } else if (!response) {
            setError("There are no response from server, please try again later.");
            setIsLoading(false);
            setLoginErrorDialogOpen(true);
            console.error('Failed to login');
        } else {
            setError("We are not able to log you in, please check your email and password.");
            setIsLoading(false);
            setLoginErrorDialogOpen(true);
            console.error('Failed to login');
        }
    };

    const validateEmail = (email: string) => {
        // Simple validation check for email pattern
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    return (
        <Box>
            <Typography
                style={{
                    color: 'rgba(240,240,240,255)',
                    left: 0,
                    fontSize: '60px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                Bonjour
                こんにちは
                Hej
                Ciao
                안녕하세요
                Здравствуйте
                Dobrý deň
                Γεια σας
                Olá
                Hej med dig
                Halo
                Merhaba
            </Typography>
            <Typography
                style={{
                    color: 'rgba(240,240,240,255)',
                    right: 0,
                    bottom: 0,
                    fontSize: '90px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                Hola
                Hei
                Hello
                Pozdravljeni
                Witam
                Sveiki
                你好
                Bună ziua
                Hallo
            </Typography>
            <Container component="main" maxWidth="xs">
                <Box display="flex" height="100vh" alignItems="center" justifyContent="center">
                    <Paper elevation={3}>
                        <Box margin="4vw" display="flex" flexDirection="column" alignItems="center"
                             justifyContent="center">
                            <form onSubmit={handleSubmit}>
                                <Grid container spacing={2} sx={{color: 'rgba(255,255,255,255)', position: 'sticky'}}>
                                    <Box display="flex" justifyContent="center">
                                        <img src={UTCLogo} alt="utcLogo" width="200"/>
                                        <img src={Logo} alt="Logo" width="200"/>
                                    </Box>
                                    <Grid item xs={12}>
                                        <TextField
                                            variant="outlined"
                                            required
                                            fullWidth
                                            id="email"
                                            label="Email Address"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                            value={email}
                                            onChange={handleEmailChange}
                                            error={emailError}
                                            helperText={emailError ? "Please enter a valid email address." : ""}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControl fullWidth variant="outlined">
                                    <InputLabel error={passwordError}
                                                htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        required
                                        id="outlined-adornment-password"
                                        name="password"
                                        label="Password"
                                        type={showPassword ? 'text' : 'password'}
                                        onChange={handlePasswordChange}
                                        error={passwordError}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff/> : <Visibility/>}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                    <FormHelperText error id="accountId-error">
                                        {passwordError ? "Password cannot be empty." : ""}
                                    </FormHelperText>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                                <Box display="flex" alignItems="center">
                                    <Typography color='black'>No account yet ?</Typography><Link ml="1vh"
                                                                                                 href={APP_ROUTES.REGISTER}>Register</Link>
                                </Box>
                            </Grid>
                            <Grid item xs={12}>
                                <Button type="submit" fullWidth variant="contained" color="primary"
                                        disabled={emailError || passwordError || email === '' || password === ''}>
                                    {isLoading ? (
                                        <CircularProgress size={24} color="inherit"/>
                                    ) : (
                                        'Sign in'
                                    )}
                                </Button>
                                <AlertDialog dialogTitle="Login failed" dialogBody={error} open={loginErrorDialogOpen}
                                             setOpen={setLoginErrorDialogOpen}/>
                            </Grid>
                                </Grid>
                            </form>
                        </Box>
                    </Paper>
                </Box>
            </Container>
        </Box>
    );
};

export default Login;
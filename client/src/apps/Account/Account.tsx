import * as React from 'react';
import {Box} from "@mui/system";
import Grid from "@mui/material/Grid";
import {
    CircularProgress,
    Paper
} from "@mui/material";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import PasswordIcon from '@mui/icons-material/Password';
import TextField from "@mui/material/TextField";
import {useEffect, useState} from "react";
import CustomPage from "../../components/organism/CustomPage";
import PasswordChecklist from "react-password-checklist"
import Button from "@mui/material/Button";
import {APP_ROUTES} from "../../router/Routes";
import {useNavigate} from "react-router-dom";
import AlertDialog from "../../components/organism/AlertDialog";

const Account: React.FC = () => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [originalFirstName, setOriginalFirstName] = useState('');
    const [originalLastName, setOriginalLastName] = useState('');
    const [email, setEmail] = useState('');
    const [usertype, setUsertype] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [oldPassword, setOldPassword] = useState("");
    const [password, setPassword] = useState("");
    const [passwordAgain, setPasswordAgain] = useState("");
    const [allowPasswordSubmit, setAllowPasswordSubmit] = useState(false);
    const [infoSubmitDisable, setInfoSubmitDisable] = useState(false);
    const [error, setError] = useState("");
    const [infoButtonIsLoading, setInfoButtonIsLoading] = useState(false);
    const [passwordButtonIsLoading, setPasswordButtonIsLoading] = useState(false);
    const [errorDialogOpen, setErrorDialogOpen] = useState(false);
    const [infoSubmitSuccessDialogOpen, setInfoSubmitSuccessDialogOpen] = useState(false);
    const [passwordSubmitSuccessDialogOpen, setPasswordSubmitSuccessDialogOpen] = useState(false);

    const navigate = useNavigate();

    const handleFirstNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFirstName(event.target.value);
    };

    const handleLastNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setLastName(event.target.value);
    };
    const handleOldPasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setOldPassword(event.target.value);
    };
    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };
    const handlePasswordAgainChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPasswordAgain(event.target.value);
    };

    const checkUpdateInfoSubmitDisable = () => {
        if (originalFirstName === firstName && originalLastName === lastName)
            setInfoSubmitDisable(true)
        else
            setInfoSubmitDisable(false)
    }

    const handleUpdateInfoSubmit = async () => {
        setInfoButtonIsLoading(true);
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/users/modify_info/` + localStorage.getItem('email'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
            }),
        }).catch((error: any) => {
            setError(error);
            setInfoButtonIsLoading(false);
            setErrorDialogOpen(true);
        });

        if (response && response.ok) {
            setInfoButtonIsLoading(false);
            setInfoSubmitSuccessDialogOpen(true);
        } else if (!response) {
            setError("There are no response from server, please try again later.");
            setInfoButtonIsLoading(false);
            setErrorDialogOpen(true);
            console.error('Failed to update');
        } else {
            setError("We are unable to process your update request, please try again later.");
            setInfoButtonIsLoading(false);
            setErrorDialogOpen(true);
            console.error('Failed to update');
        }
    }

    const handleChangePasswordSubmit = async () => {
        setPasswordButtonIsLoading(true);
        const response = await fetch(`${process.env.REACT_APP_API_BASE_URL}/users/modify_password/` + localStorage.getItem('email'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                oldPassword: oldPassword,
                password: password,
            }),
        }).catch((error: any) => {
            setError(error);
            setPasswordButtonIsLoading(false);
            setErrorDialogOpen(true);
        });

        if (response && response.ok) {
            setPasswordButtonIsLoading(false);
            setPasswordSubmitSuccessDialogOpen(true);
        } else if (!response) {
            setError("There are no response from server, please try again later.");
            setPasswordButtonIsLoading(false);
            setErrorDialogOpen(true);
            console.error('Failed to change password');
        } else {
            setError("We are unable to process your password change request, please check your password and try again later.");
            setPasswordButtonIsLoading(false);
            setErrorDialogOpen(true);
            console.error('Failed to change password');
        }
    }

    const fetchUserData = () => {
        fetch("http://localhost:8080/api/users/" + localStorage.getItem('email'), {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }),
        })
            .then((res) => {
                if (res.status === 401 || res.statusText === "TOKEN_EXPIRED") {
                    localStorage.removeItem('email');
                    localStorage.removeItem('token');
                    navigate(APP_ROUTES.LOGIN);
                    return [];
                }
                return res.json();
            })
            .then((result) => {
                console.log(result)
                setEmail(result.mail);
                setLastName(result.lastName);
                setFirstName(result.firstName);
                setOriginalLastName(result.lastName);
                setOriginalFirstName(result.firstName);
                setUsertype(result.admin ? "admin" : "user")
                setIsLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setIsLoading(false);
            });
    };

    useEffect(() => {
        checkUpdateInfoSubmitDisable();
    }, [lastName, firstName])

    useEffect(() => {
        setIsLoading(true);
        fetchUserData();
    }, [navigate]);


    return (
        <CustomPage>
            {isLoading ? <Box display="flex" alignItems="center" justifyContent="center"><CircularProgress/></Box> :
                <Box display="row" marginTop="10vh">
                    <Box display="flex" alignItems="center" justifyContent="center">
                        <Paper elevation={3} sx={{width: "50vw"}}>
                            <Box margin="2vw">
                                <Grid item xs={12}>
                                    <Box display="flex" justifyContent="center">
                                        <AccountCircleIcon sx={{fontSize: "50px", color: "grey"}}/>
                                    </Box>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="standard"
                                        disabled
                                        fullWidth
                                        id="email"
                                        label="Email"
                                        name="email"
                                        value={email}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="standard"
                                        disabled
                                        fullWidth
                                        id="usertype"
                                        label="User Type"
                                        name="usertype"
                                        value={usertype}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="standard"
                                        required
                                        fullWidth
                                        id="firstName"
                                        label="First Name"
                                        name="firstName"
                                        autoComplete="firstName"
                                        value={firstName}
                                        onChange={handleFirstNameChange}
                                        helperText={firstName == null || firstName === '' ? "Please enter a valid name." : ""}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="standard"
                                        required
                                        fullWidth
                                        id="lastName"
                                        label="Last Name"
                                        name="lastName"
                                        autoComplete="lastName"
                                        value={lastName}
                                        onChange={handleLastNameChange}
                                        helperText={lastName == null || lastName === '' ? "Please enter a valid name." : ""}
                                    />
                                </Grid>
                                <Box height="2vh"/>
                                <Box display="flex" justifyContent="center">
                                    <Box width="20vw">
                                        <Button fullWidth type="submit" variant="contained" color="primary"
                                                disabled={infoSubmitDisable} onClick={handleUpdateInfoSubmit}>
                                            {infoButtonIsLoading ? (
                                                <CircularProgress size={24} color="inherit"/>
                                            ) : (
                                                'Update Info'
                                            )}
                                        </Button>
                                    </Box>
                                </Box>
                            </Box>
                        </Paper>
                    </Box>
                    <Box height="5vh"/>
                    <Box display="flex" alignItems="center" justifyContent="center">
                        <Paper elevation={3} sx={{width: "50vw"}}>
                            <Box margin="2vw">
                                <Grid item xs={12}>
                                    <Box display="flex" justifyContent="center">
                                        <PasswordIcon sx={{fontSize: "50px", color: "grey"}}/>
                                    </Box>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="standard"
                                        fullWidth
                                        type="password"
                                        id="oldPassword"
                                        label="Old Password"
                                        name="oldPassword"
                                        value={oldPassword}
                                        onChange={handleOldPasswordChange}
                                    />
                                    <TextField
                                        variant="standard"
                                        fullWidth
                                        type="password"
                                        id="password"
                                        label="New Password"
                                        name="password"
                                        value={password}
                                        onChange={handlePasswordChange}
                                    />
                                    <TextField
                                        variant="standard"
                                        fullWidth
                                        type="password"
                                        id="passwordAgain"
                                        label="Confirm New Password"
                                        name="passwordAgain"
                                        value={passwordAgain}
                                        onChange={handlePasswordAgainChange}
                                    />
                                    {(password !== '' || passwordAgain !== '') &&
                                        <Box>
                                            <PasswordChecklist
                                                rules={["minLength", "specialChar", "number", "capital", "match"]}
                                                minLength={5}
                                                value={password}
                                                valueAgain={passwordAgain}
                                                onChange={(isValid) => {
                                                    if (isValid) {
                                                        setAllowPasswordSubmit(true)
                                                    } else {
                                                        setAllowPasswordSubmit(false)
                                                    }
                                                }}
                                            />
                                        </Box>
                                    }
                                    <Box height="2vh"/>
                                    <Box display="flex" justifyContent="center">
                                        <Box width="20vw">
                                            <Button fullWidth type="submit" variant="contained" color="warning"
                                                    disabled={!allowPasswordSubmit}
                                                    onClick={handleChangePasswordSubmit}>
                                                {passwordButtonIsLoading ? (
                                                    <CircularProgress size={24} color="inherit"/>
                                                ) : (
                                                    'Change Password'
                                                )}
                                            </Button>
                                        </Box>
                                    </Box>
                                </Grid>
                            </Box>
                        </Paper>
                    </Box>
                </Box>
            }
            <AlertDialog open={infoSubmitSuccessDialogOpen} setOpen={setInfoSubmitSuccessDialogOpen}
                         dialogTitle={"Update success"} dialogBody={"Your infomation has been updated successfully"}
                         refreshOnClose={true}/>
            <AlertDialog open={passwordSubmitSuccessDialogOpen} setOpen={setPasswordSubmitSuccessDialogOpen}
                         dialogTitle={"Password updated"} dialogBody={"Your password has been changed successfully"}
                         refreshOnClose={true}/>
            <AlertDialog dialogTitle={"Something went wrong ..."} dialogBody={error} open={errorDialogOpen}
                         setOpen={setErrorDialogOpen} refreshOnClose={true}/>
        </CustomPage>
    );
}

export default Account;
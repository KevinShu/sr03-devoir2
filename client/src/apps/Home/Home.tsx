import React from 'react';
import {CurrentDateTime} from "components/molecule/CurrentTime";
import ChatIcon from '@mui/icons-material/Chat';
import PeopleIcon from '@mui/icons-material/People';
import SettingsIcon from '@mui/icons-material/Settings';
import {IconButton, Paper, Typography} from "@mui/material";
import {Box} from "@mui/system";
import {useNavigate} from "react-router-dom";
import CustomPage from "../../components/organism/CustomPage";

const Home: React.FC = () => {
    const email = localStorage.getItem('email')
    const navigate = useNavigate()
    return (
        <CustomPage>
            <Typography
                style={{
                    color: '#f5f5f5',
                    left: 0,
                    fontSize: '160px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                BIENVENUE
            </Typography>
            <Typography
                style={{
                    color: '#f0f0f0',
                    left: 0,
                    fontSize: '50px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                {email}
            </Typography>
            <Typography
                style={{
                    color: '#e0e0e0',
                    left: 0,
                    bottom: 0,
                    fontSize: '30px',
                    position: 'absolute',
                    zIndex: -1,
                }}
            >
                Il est <CurrentDateTime/>
            </Typography>
            {/*<Typography variant="h2">*/}
            {/*    Bienvenue, {email}*/}
            {/*</Typography>*/}
            {/*<Typography variant="h2">*/}
            {/*    Il est <CurrentDateTime/>*/}
            {/*</Typography>*/}
            <Box display="flex" alignContent="center" alignItems="center" justifyContent="space-around" height="90vh"
                 width="100vh"
                 mt="5vh">
                <Paper elevation={8} onClick={() => {
                    navigate('/chat')
                }}>
                    <Box margin="2vw" display="row">
                        <IconButton color="primary" aria-label="chat">
                            <ChatIcon sx={{fontSize: '70px'}}/>
                        </IconButton>
                        <Typography textAlign="center">
                            Chat
                        </Typography>
                    </Box>
                </Paper>
                <Paper elevation={8} onClick={() => {
                    navigate('/contact')
                }}>
                    <Box margin="2vw" display="row">
                        <IconButton color="primary" aria-label="people">
                            <PeopleIcon sx={{fontSize: '70px'}}/>
                        </IconButton>
                        <Typography textAlign="center">
                            Contact
                        </Typography>
                    </Box>
                </Paper>
                <Paper elevation={8} onClick={() => {
                    navigate('/settings')
                }}>
                    <Box margin="2vw" display="row">
                        <IconButton color="primary" aria-label="settings">
                            <SettingsIcon sx={{fontSize: '70px'}}/>
                        </IconButton>
                        <Typography textAlign="center">
                            Settings
                        </Typography>
                    </Box>
                </Paper>
            </Box>
        </CustomPage>
    );
};

export default Home;
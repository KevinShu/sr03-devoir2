import {Routes, Route} from 'react-router-dom'
import {APP_ROUTES} from 'router/Routes'
import PrivateRoute from 'components/atom/PrivateRoute'
import PublicRoute from 'components/atom/PublicRoute'
import Home from 'apps/Home/Home'
import Login from 'apps/Login/Login'
import Contact from 'apps/Contact/Contact'
import Chat from 'apps/Chat/Chat'
import Settings from 'apps/Settings/Settings'
import Register from 'apps/Register/Register'
import Account from "apps/Account/Account";

const AppRouter = () => {
    return (
        <Routes>
            <Route
                path={APP_ROUTES.LOGIN}
                element={
                    <PublicRoute>
                        <Login/>
                    </PublicRoute>
                }
            />
            <Route
                path={APP_ROUTES.REGISTER}
                element={
                    <PublicRoute>
                        <Register/>
                    </PublicRoute>
                }
            />
            <Route
                path={APP_ROUTES.ROOT}
                element={
                    <PrivateRoute>
                        <Home/>
                    </PrivateRoute>
                }
            />
            <Route
                path={APP_ROUTES.HOME}
                element={
                    <PrivateRoute>
                        <Home/>
                    </PrivateRoute>
                }
            />
            <Route
                path={APP_ROUTES.ACCOUNT}
                element={
                    <PrivateRoute>
                        <Account/>
                    </PrivateRoute>
                }
            />
            <Route
                path={APP_ROUTES.CONTACT}
                element={
                    <PrivateRoute>
                        <Contact/>
                    </PrivateRoute>
                }
            />
            <Route
                path={APP_ROUTES.SETTINGS}
                element={
                    <PrivateRoute>
                        <Settings/>
                    </PrivateRoute>
                }
            />
            <Route
                path={APP_ROUTES.CHAT}
                element={
                    <PrivateRoute>
                        <Chat/>
                    </PrivateRoute>
                }
            />
        </Routes>
    )
}

export default AppRouter
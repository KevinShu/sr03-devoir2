export enum APP_ROUTES {
    ROOT = '/',
    LOGIN = '/login',
    HOME = '/home',
    CHAT = '/chat',
    CONTACT = '/contact',
    SETTINGS = '/settings',
    REGISTER = '/register',
    ACCOUNT = '/account',
}
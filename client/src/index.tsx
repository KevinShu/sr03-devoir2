import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {BrowserRouter} from "react-router-dom";
import AppRouter from 'router/AppRouter'
import TopNavbarDesktop from "components/organism/TopNavBar";

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    /*
    * In development mode and strict mode, all useEffect() will be executed twice,
    * StrictMode is temporarily disabled until final deployment
    * */
    // <React.StrictMode>
    <BrowserRouter>
        <TopNavbarDesktop/>
        <AppRouter/>
    </BrowserRouter>
    // </React.StrictMode>
);

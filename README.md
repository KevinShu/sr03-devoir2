## SR03 ChatApp

|  Information |   Valeur              |
| :------------ | :------------- |
| **Auteurs**     | Bingqian SHU (bingqian.shu@etu.utc.fr)     |
|                 | Houze ZHANG (houze.zhang@etu.utc.fr)       |
| **Encadrants**  | Ahmed LOUNIS (ahmed.lounis@hds.utc.fr)     |
|                 | Cédric MARTINET (cedric.martinet@utc.fr)   |

### Architecture de l'application

![img_2.png](img_2.png)

### La conception (diagramme de classes, schéma relationnel, justifier brièvement vos choix)

#### Database

![img.png](doc%2Fimg%2Fimg.png)

#### Dao

![dao.png](doc%2Fimg%2Fdao.png)

Ces quatre classes Dao sont utilisées pour effectuer des requêtes personnalisées. Cependant, il n'y a pas de Dao correspondant pour la table "conversation_user", qui est une table de clé étrangère pure.

#### Model

![model.png](doc%2Fimg%2Fmodel.png)

Ces quatre classes dans le cadre de l'ORM représentent les enregistrements de la base de données et les associent à des objets.

#### Contrôleurs (sans Rest)

![controller.png](doc%2Fimg%2Fcontroller.png)

Ces classes de contrôleurs, basées sur Spring MVC et le mécanisme de session Spring MVC, sont responsables du traitement des requêtes et du renvoi des pages Web au client.

#### Contrôleur (Restful)

![restController.png](doc%2Fimg%2FrestController.png)

Ces contrôleurs, basés sur le style REST, sont utilisés pour répondre aux demandes de pages utilisateur.

#### Services

![service.png](doc%2Fimg%2Fservice.png)

Les services de messagerie sont séparés en services distincts pour être utilisés par d'autres classes.

### Explications sur les interactions entre les différentes technologies : React, Spring et WebSocket

- Le cycle de vie des beans est géré par le conteneur IoC fourni par le framework Spring. Ils sont créés et contrôlés par Spring Boot, chargés dans la mémoire JVM du serveur et détruits lorsque le serveur est arrêté.
- Les objets beans sont injectés automatiquement grâce à la fonctionnalité d'injection de dépendances de Spring Framework. Cette fonctionnalité d'injection de dépendances est largement utilisée dans nos couches de contrôleurs (Controller) et de modèles (Model).
- Le client envoie des requêtes de page au serveur via une URL, et les beans de classe Controller définis par Spring MVC répondent à ces requêtes (REST ou non-REST).
  - Pour les requêtes non-REST, le mécanisme de session de Spring MVC est utilisé pour maintenir l'état de la session HTTP.
  - Pour les requêtes REST, le `WebSocket` est utilisé pour établir une session HTTP persistante, afin de mettre en œuvre un mécanisme de session pour les requêtes REST sans état.

### Résultat eco-index de votre site (plugin greenit) et des pistes d'amélioration

| Date               | Url                                         | Request number | Size(kb) | Dom size | Greenhouse Gases Emission (gCO2e) | Water consumption | ecoIndex | Grade |
| ------------------ |---------------------------------------------| -------------- | -------- | -------- | --------------------------------- | ----------------- | -------- | ----- |
| 2023/6/16 14:44:17 | "http://localhost:3000/contact"             | 31             | 1376     | 852      | 1.94                              | 2.90              | 53.17    | D     |
| 2023/6/16 14:43:56 | "http://localhost:3000/home"                | 26             | 1374     | 1241     | 2.06                              | 3.10              | 46.78    | D     |
| 2023/6/16 14:43:31 | "http://localhost:3000/register"            | 24             | 1373     | 1179     | 2.04                              | 3.06              | 48.08    | D     |
| 2023/6/16 14:43:16 | "http://localhost:3000/settings"            | 16             | 688      | 621      | 1.67                              | 2.51              | 66.39    | C     |
| 2023/6/16 14:43:05 | "http://localhost:3000/login"               | 14             | 687      | 689      | 1.71                              | 2.57              | 64.35    | C     |
| 2023/6/16 14:42:59 | "http://localhost:3000/account"             | 10             | 685      | 673      | 1.69                              | 2.54              | 65.38    | C     |
| 2023/6/16 14:42:52 | "http://localhost:3000/chat"                | 4              | 1        | 380      | 1.37                              | 2.05              | 81.54    | A     |
| 2023/6/16 14:42:30 | ""                                          | 0              | 0        | 217      | 1.19                              | 1.78              | 90.54    | A     |
| 2023/6/16 14:35:17 | "http://localhost:8080/admin/status"        | 10             | 668      | 149      | 1.29                              | 1.93              | 85.74    | A     |
| 2023/6/16 14:35:08 | "http://localhost:8080/admin/messages"      | 7              | 2003     | 645      | 1.78                              | 2.68              | 60.82    | C     |
| 2023/6/16 14:34:57 | "http://localhost:8080/admin/conversations" | 3              | 322      | 219      | 1.28                              | 1.92              | 86.17    | A     |
| 2023/6/16 14:34:50 | "http://localhost:8080/admin/users"         | 9              | 804      | 375      | 1.47                              | 2.20              | 76.60    | B     |
| 2023/6/16 14:34:32 | "http://localhost:8080/admin/users/add"     | 4              | 318      | 75       | 1.19                              | 1.78              | 90.58    | A     |
| 2023/6/16 14:34:13 | "http://localhost:8080/login"               | 33             | 5395     | 43       | 1.48                              | 2.21              | 76.23    | B     |

Nous pouvons tirer les principales observations suivantes :
- Les pages "http://localhost:3000/contact", "http://localhost:3000/home" et "http://localhost:3000/register" ont des émissions de gaz à effet de serre et une consommation d'eau relativement élevées, et elles obtiennent un grade D. Il est possible d'optimiser ces pages pour réduire leur impact sur l'environnement.
- Les pages "http://localhost:3000/settings", "http://localhost:3000/login" et "http://localhost:3000/account" obtiennent un grade C, avec une amélioration légère en termes d'émissions de gaz à effet de serre et de consommation d'eau par rapport aux pages précédentes.
- La page "http://localhost:3000/chat" obtient le meilleur grade, A, avec des émissions de gaz à effet de serre et une consommation d'eau relativement faibles. Cependant, sa taille de fichier est très petite (1 Ko), ce qui pourrait être une erreur de mesure.
- Les pages "http://localhost:8080/admin/status" et "http://localhost:8080/admin/users/add" obtiennent également un grade A et affichent de bonnes performances en termes d'impact environnemental.
- Les pages "http://localhost:8080/admin/messages", "http://localhost:8080/admin/conversations" et "http://localhost:8080/login" ont des grades plus bas, allant de C à B.
- Dans l'ensemble, il y a des améliorations possibles en termes d'impact environnemental des pages frontales. Il est possible d'améliorer les performances en réduisant le nombre de requêtes, en optimisant la taille des fichiers et en améliorant le code pour réduire l'impact sur l'environnement.